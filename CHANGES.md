# Changes

## 2023-02-23 - Nima Badizadegan

Initial release under CERN-OHL-W.  R5Lite should be considered a "beta" version, with basic core features working, but
missing "quality of life" features, serious testing, and an "SoC builder" script that should make life easier.