# R5Lite RISC-V CPU Core (RV32IB)

## Description

R5Lite is a RISC-V RV32IB microcontroller core designed for running integer state machines inside FPGAs and silicon
chips.  The core uses a simple 3-stage pipeline that allows most instructions to have single-cycle execution at a
reasonable clock speed.  This is intended to be an auxiliary real-time core rather than a main general-purpose core
for a microcontroller or microprocessor.  Intended applications include:

* Bit banging low- or medium-speed I/O protocols such as I2C, CAN, or weirder protocols from within an FPGA
* Running state machines that control more complicated hardware blocks
* Simple computational tasks that do not require a full-sized microprocessor or custom logic

The bitmanip and integer instruction sets are available because those are the instruction sets that are most likely
to be used for simple I/O and basic control applications.  The M instruction set extension, adding MUL and DIV, is
planned to help with more complicated integer code that may be involved in control and I/O state machines, and the
CLMUL instruction set may also be added to help with error-correcting codes.

The other parts of the RISC-V "G" instruction sets (the A, F, and D extensions) are not planned at this time.

### Why Make Your Own Core?

I started this project related to my startup, where it will serve as a control processor for some cryptographic
systems.  I wanted a few weird features (eg constant-time division) that I would have had to hack into other cores
anyway, and I wanted the B instruction set because it gives my application a meaningful performance uplift.  I also
didn't see a Cortex-M3-style RISC-V core out there - most of the better-tested open-source RISC-V cores are larger
designs with more pipeline stages and bigger ambitions.

I am not putting the IP for this core underneath the startup - I don't want the core to be abandoned if the startup
gets acquired (wishful thinking) or go into limbo if it goes bankrupt (much more realistic).  Since the company is
bootstrapped, I have the freedom to do this, so I am doing it.

### RISC-V Specifications

For the base RISC-V ISA, this core follows version 2.1 of the RV32I specification.  

The applicable specification documents from the RISC-V foundation are the following:

* [Volume I, Unprivileged Specification version 20191213](https://github.com/riscv/riscv-isa-manual/releases/download/Ratified-IMAFDQC/riscv-spec-20191213.pdf) (Compiled December 2019)
* [RISC-V Bit Manipulation ISA-extensions](https://github.com/riscv/riscv-bitmanip/releases/download/1.0.0/bitmanip-1.0.0-38-g865e7a7.pdf) (Ratified November 2021)

The R5Lite core implements the following instruction set extensions

* Base ISA: RV32I
* Bit manipulation instructions: Zba, Zbb, Zbs

We plan to add support for the following instruction set extensions

* Multiplication/division (will be a parameter option): M
* Compressed instructions (will likely not be optional at the RTL level): C

The following instruction sets may also be added as parameter options:

* Control and Status registers: Zicsr
* Carryless multiplication (useful for some error-correcting codes and cryptography): Zbc
* Some fraction of the cryptography extension, as parameter options

## Architecture

Currently, R5Lite is a RV32IB core, with plans to add the M and C extensions.  It is designed primarily for
integer workloads that are involved in being a "state machine controller" for silicon and FPGA systems rather than
a general-purpose microcontroller core.  Currently, it does not support any of the privileged instruction set
either - the CPU runs in only one mode.

The R5Lite core uses a 3-stage pipeline, consisting of fetch, decode, and execute stages.  It has a simple BTFNT
(backward taken, forward not taken) branch predictor, and it is capable of accepting variable latency on both the
program and data memory buses.

The fetch stage issues read commands to the instruction memory.  Most of the time in this stage is intended to be
spent in the memory system.  The R5Lite core can acept variable latency here, so large programs can be run with
caching.  However, we require that the memory used have a minimum of 1 cycle of latency.

After the fetch stage is the decode stage, where the instruction is decoded, the branch predictor predicts
a new PC, the register file is read, and a command is issued to the execute stage.  At this stage, the pipeline may
stall for multi-cycle instructions (currently only LOAD instructions, but DIV will also stall here).

The execute stage does the heavy lifting.  It consists of a shifter and an ALU in sequence, allowing us to execute
normal arithmetic and logic instructions as well as the more complicated bit manipulation instructions present in the
B instruction set.  It contains the data memory port to issue loads and stores.  A multiplier and a division
approximation circuit are planned to be added to the execution engine (in parallel with the SHIFT-ALU system) in
parallel with the SHIFT-ALU system, allowing us to cover all of the basic ALU operations.  The execution stage is
"dumb" and has only a small set of multiplexers and a bypass register in addition to the ALU and shifter.

### Multi-Cycle and Variable-Latency Instructions

Almost all instructions, including all arithmetic and bit manipulation instructions, are single-cycle.  The current
exceptions to this are control flow instructions.  Division operations will also be multi-cycle in the future. JAL
instructions have single-cycle latency.  Branch instructions are single-cycle, but have a 1-cycle mispredict penalty.
JALR instructions incur 2 cycles of latency, and are essentially treated as always-mispredicted branches.

The core is equipped with bypass paths, so back-to-back arithemtic operations on the same register incur no penalty,
even though register values are fetched in the decode stage.

### Memory Access Stalls

The core can only tolerate having a single outstanding load.  Thus, load instructions may stall the pipeline if:

* Another load instruction is issued before the previous load completes
* An instruction attempts to use (either as a source or destination operand) a loaded value before the load completes

This means that loads can be mostly considered single-cycle instructions if you can cover the latency of the load
with other unrelated instructions.  If you use the loaded value or attempt to overwrite the destination register, the
pipeline will stall until the load completes.

Store instructions are single-cycle, and currently have no limit on the number of outstanding stores.  There is
currently no notion of "retirement" of a store command on the memory bus, and store instructions are treated as
retiring immediately, although this *will* change when bus interfaces are added for AHB and AXI-4 lite - the internal
bus interface will be augmented with an `ack` signal to handle this.

### Variable Memory Latency

The core supports variable memory latency on the instruction and data memory buses, so long as that latency is bounded.

The instruction fetch unit has logic to absorb slack in memory access times of up to 8 cycles.  This comes in the form
of a small flushable FIFO inside the Fetch unit.  If you have more than 8 cycles of instruction fetch latency in the
worst case, you will want to increase the size of this FIFO.  The FIFO is flushed on branch misprediction, so you do
not have old instructions clogging the FIFO if a mispredict happens to occur (although the core expects reads to retire
in order, so outstanding reads will cause extra latency in the case of a misprediction).

On the data memory bus, the core can accept unbounded read latency, but will stall if a load is very slow to return.
There is currently no timeout on how long a load can take, so a load that fails to return will stall the core
indefinitely.

## Language and Tooling Choices

The core is in SystemVerilog, and we use Verilator and C++ testbenches for testing.  The intention of the core is that
it can be compiled in all of the proprietary FPGA and ASIC toolchains as well as an open-source toolchain or two, but 
these have not been tested or verified.  However, the core uses a very limited subset of SystemVerilog syntax to
accomplish this goal.  The features that tools will need to accept include:

* Packages
* SystemVerilog data types (`logic` and `bit`)
* Decorated always blocks (`always_comb` and `always_ff`)
* Packed structs
* Enums
* Automatic functions
* Module parametrization
* For loops in always blocks

As far as I know, these features are provided by pretty much every tool with basic SystemVerilog support.  This list may
shrink if that is not the case.

Features that are intentionally avoided are:

* Unions
* Interfaces
* Anything in the "simulation" part of the language (constrained random stuff, classes, etc.)
* Built-in tasks/functions (aside from `$clog2`)

The tools on which synthesis and simulation have been tested are:

* Intel Quartus
* Xilinx Vivado
* Verilator

## Planned Features

This core is still a work in progress, and has several steps before I would consider it to be "released."  Today, I
would suggest that it is usable for academic experiments, but not for production use.

The following features are definitely planned to be added to this core:

* Multiply and divide (using a fast division algorithm)
* Compressed instructions
* An interrupt system
* Adapters from the internal memory bus to AHB and AXI-4
* An "SoC Builder" script that creates a simple Harvard-architecture system from a RISC-V ELF file and a system spec

The following features may also be planned:

* Trapping on bad PC values or bad instructions (currently, the core assumes that code it is running is well-formed)
* A load timeout and/or a watchdog clock
* Adding a few CSRs, including time/cycle counters
* Adding a tightly-coupled I/O interface

I am also planning on testing the core FAR more thoroughly than it is currently tested.

## Contributing

No pull requests please.  This code is part of a commercial project for my company, [Arbitrand](https://arbitrand.com),
and it wouldn't be right to ask you to give me a commercial license of code that you contribute for free.

If you are interested in helping out, please help by using and stress-testing the design, and letting me know about any
bugs that show up.  These are always welcome, and will help make the core both more secure and more usable for myself
and others.

## License

Code in this repository is licensed under CERN-OHL-W v2.

An alternative commercial license for a modest fee ($5,000 or less) may be available once the code is better tested.
