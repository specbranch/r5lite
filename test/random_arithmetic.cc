// Random Arithmetic Testbench
//
// Test straight-line programs with arithmetic opcodes only, but a lot of random operations.  This
// test is designed to stress the arithemtic parts of the backend as well as the bypassing logic.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#include <gtest/gtest.h>

#include <cstdint>
#include <cstdlib>
#include <random>
#include <vector>

#include "risc_test.hpp"
#include "risc_inst.hpp"

class RandomArithmetic : public RiscTest {
  public:
    RandomArithmetic () : reg_gen(0, 31) {
        std::random_device rd;
        gen.seed(rd());
    }

    void MakeProgramRegs() {
        // Make initial register states
        expected_regs.emplace_back(0);
        for (int i = 1; i < 32; i++) {
            uint32_t reg_value = gen();

            expected_regs.emplace_back(reg_value);

            // Generate an LUI + ADDI pair, but make sure we do it correctly
            uint32_t reg_value_top = reg_value >> 12;
            uint32_t reg_value_bottom = reg_value & ((1 << 12) - 1);

            // If our ADDI is going to subtract, increment the LUI value
            if ((reg_value_bottom >> 11) != 0) reg_value_top += 1;

            // Code to generate the register value
            if (reg_value_top != 0) {
                uint32_t lui = rv::MakeImmediate(reg_value_top, rv::BaseOps::LUI);
                lui |= i << rv::DEST;
                program.emplace_back(lui);
            }

            uint32_t addi = rv::MakeImmediate(reg_value_bottom, rv::BaseOps::ADDI);
            addi |= i << rv::OPERAND_A;
            addi |= i << rv::DEST;
            program.emplace_back(addi);
        }
    }

    void MakeProgramEnd() {
        program.emplace_back(rv::BaseOps::EBREAK);
        program.emplace_back(rv::BaseOps::ADDI);
    }

    void RunProgram() {
        Run(program, 0, program.size());
    }

    void Check() {
        CheckRegs(expected_regs);
    }

    void RandomBasicImm() {
        std::uniform_int_distribution<std::size_t> imms(0, BASIC_IMM.size() - 1);
        uint32_t opcode = BASIC_IMM[imms(gen)];

        uint32_t dest = reg_gen(gen);
        uint32_t src = reg_gen(gen);
        int immediate = gen() & ((1 << rv::ImmediateBits(opcode)) - 1);

        // Make the instruction
        uint32_t instruction = rv::MakeImmediate(immediate, opcode);
        instruction |= src << rv::OPERAND_A;
        instruction |= dest << rv::DEST;

        program.emplace_back(instruction);

        // Simulate the instruction
        if (dest != 0) {
            // Sign-extend the immediate if immediate is 12 bits
            immediate = (immediate << 20) >> 20;
            uint32_t uimm = immediate;

            uint32_t reg_data = expected_regs[src];
            int reg_data_signed = reg_data;

            uint32_t result;
            switch(opcode) {
                case rv::BaseOps::ADDI  : result = reg_data + immediate; break;
                case rv::BaseOps::SLTI  : result = (reg_data_signed < immediate) ? 1 : 0; break;
                case rv::BaseOps::SLTIU : result = (reg_data < uimm) ? 1 : 0; break;
                case rv::BaseOps::XORI  : result = reg_data ^ uimm; break;
                case rv::BaseOps::ORI   : result = reg_data | uimm; break;
                case rv::BaseOps::ANDI  : result = reg_data & uimm; break;
                case rv::BaseOps::SLLI  : result = reg_data << uimm; break;
                case rv::BaseOps::SRLI  : result = reg_data >> uimm; break;
                case rv::BaseOps::SRAI  : result = reg_data_signed >> immediate; break;
            }

            expected_regs[dest] = result;
        }
    }

    void RandomBasicReg() {
        std::uniform_int_distribution<std::size_t> reg_ops(0, BASIC_RR.size() - 1);
        uint32_t opcode = BASIC_RR[reg_ops(gen)];

        uint32_t dest = reg_gen(gen);
        uint32_t src_a = reg_gen(gen);
        uint32_t src_b = reg_gen(gen);

        // Make the instruction
        uint32_t instruction = opcode | (src_b << rv::OPERAND_B);
        instruction |= src_a << rv::OPERAND_A;
        instruction |= dest << rv::DEST;

        program.emplace_back(instruction);

        // Simulate the instruction
        if (dest != 0) {
            uint32_t a_uns = expected_regs[src_a];
            int a_signed = a_uns;
            uint32_t b_uns = expected_regs[src_b];
            int b_signed = b_uns;
            uint32_t b_mask = b_uns & ((1 << 5) - 1);

            uint32_t result;
            switch(opcode) {
                case rv::BaseOps::ADD  : result = a_uns + b_uns; break;
                case rv::BaseOps::SUB  : result = a_uns - b_uns; break;
                case rv::BaseOps::SLT  : result = (a_signed < b_signed) ? 1 : 0; break;
                case rv::BaseOps::SLTU : result = (a_uns < b_uns) ? 1 : 0; break;
                case rv::BaseOps::XOR  : result = a_uns ^ b_uns; break;
                case rv::BaseOps::OR   : result = a_uns | b_uns; break;
                case rv::BaseOps::AND  : result = a_uns & b_uns; break;
                case rv::BaseOps::SLL  : result = a_uns << b_mask; break;
                case rv::BaseOps::SRL  : result = a_uns >> b_mask; break;
                case rv::BaseOps::SRA  : result = a_signed >> static_cast<int>(b_mask); break;
            }

            expected_regs[dest] = result;
        }
    }

    void RandomBitmanipImm() {
        std::uniform_int_distribution<std::size_t> imms(0, BITMANIP_IMM.size() - 1);
        uint32_t opcode = BITMANIP_IMM[imms(gen)];

        uint32_t dest = reg_gen(gen);
        uint32_t src = reg_gen(gen);
        int immediate = gen() & ((1 << rv::ImmediateBits(opcode)) - 1);

        // Make the instruction
        uint32_t instruction = rv::MakeImmediate(immediate, opcode);
        instruction |= src << rv::OPERAND_A;
        instruction |= dest << rv::DEST;

        program.emplace_back(instruction);

        // Simulate the instruction
        if (dest != 0) {
            // Sign-extend the immediate if immediate is 12 bits
            immediate = (immediate << 20) >> 20;
            uint32_t uimm = immediate;
            uint32_t reg_data = expected_regs[src];

            uint32_t result;
            switch(opcode) {
                case rv::BitmanipOps::RORI : {
                    if (immediate == 0) {
                        result = reg_data;
                    } else {
                        result = (reg_data >> uimm) | (reg_data << (32 - uimm));
                    }
                } break;
                case rv::BitmanipOps::BEXTI : result = (reg_data >> uimm) & 1; break;
                case rv::BitmanipOps::BCLRI : result = reg_data & ~(1 << uimm); break;
                case rv::BitmanipOps::BSETI : result = reg_data | (1 << uimm); break;
                case rv::BitmanipOps::BINVI : result = reg_data ^ (1 << uimm); break;
            }

            expected_regs[dest] = result;
        }
    }

    void RandomBitmanipUnary() {
        std::uniform_int_distribution<std::size_t> unaries(0, BITMANIP_UNARY.size() - 1);
        uint32_t opcode = BITMANIP_UNARY[unaries(gen)];

        uint32_t dest = reg_gen(gen);
        uint32_t src = reg_gen(gen);

        // Make the instruction
        uint32_t instruction = opcode;
        instruction |= src << rv::OPERAND_A;
        instruction |= dest << rv::DEST;

        program.emplace_back(instruction);

        // Simulate the instruction
        if (dest != 0) {
            uint32_t reg_data = expected_regs[src];
            int signed_data = reg_data;

            uint32_t result;
            switch(opcode) {
                case rv::BitmanipOps::CLZ : {
                    if (reg_data == 0) {
                        result = 32;
                    } else {
                        result = __builtin_clz(reg_data);
                    }
                } break;
                case rv::BitmanipOps::CTZ : {
                    if (reg_data == 0) {
                        result = 32;
                    } else {
                        result = __builtin_ctz(reg_data);
                    }
                } break;
                case rv::BitmanipOps::ORCB : {
                    char* byte = reinterpret_cast<char*>(&reg_data);
                    for (int i = 0; i < 4; i++) {
                        if (byte[i] != 0) byte[i] = 0xff;
                    }
                    result = reg_data;
                } break;
                case rv::BitmanipOps::CPOP  : result = __builtin_popcount(reg_data); break;
                case rv::BitmanipOps::REV8  : result = __builtin_bswap32(reg_data); break;
                case rv::BitmanipOps::SEXTB : result = (signed_data << 24) >> 24; break;
                case rv::BitmanipOps::SEXTH : result = (signed_data << 16) >> 16; break;
                case rv::BitmanipOps::ZEXTH : result = reg_data & 0xffff; break;
            }

            expected_regs[dest] = result;
        }
    }

    void RandomBitmanipReg() {
        std::uniform_int_distribution<std::size_t> reg_ops(0, BITMANIP_RR.size() - 1);
        uint32_t opcode = BITMANIP_RR[reg_ops(gen)];

        uint32_t dest = reg_gen(gen);
        uint32_t src_a = reg_gen(gen);
        uint32_t src_b = reg_gen(gen);

        // Make the instruction
        uint32_t instruction = opcode | (src_b << rv::OPERAND_B);
        instruction |= src_a << rv::OPERAND_A;
        instruction |= dest << rv::DEST;

        program.emplace_back(instruction);

        // Simulate the instruction
        if (dest != 0) {
            uint32_t a_uns = expected_regs[src_a];
            int a_signed = a_uns;
            uint32_t b_uns = expected_regs[src_b];
            int b_signed = b_uns;
            uint32_t b_mask = b_uns & ((1 << 5) - 1);

            uint32_t result;
            switch(opcode) {
                case rv::BitmanipOps::ROR : {
                    if (b_mask == 0) {
                        result = a_uns;
                    } else {
                        result = (a_uns >> b_mask) | (a_uns << (32 - b_mask));
                    }
                } break;
                case rv::BitmanipOps::ROL : {
                    if (b_mask == 0) {
                        result = a_uns;
                    } else {
                        result = (a_uns << b_mask) | (a_uns >> (32 - b_mask));
                    }
                } break;
                case rv::BitmanipOps::BEXT   : result = (a_uns >> b_mask) & 1; break;
                case rv::BitmanipOps::BCLR   : result = a_uns & ~(1 << b_mask); break;
                case rv::BitmanipOps::BINV   : result = a_uns ^ (1 << b_mask); break;
                case rv::BitmanipOps::BSET   : result = a_uns | (1 << b_mask); break;
                case rv::BitmanipOps::XNOR   : result = a_uns ^ ~b_uns; break;
                case rv::BitmanipOps::ORN    : result = a_uns | ~b_uns; break;
                case rv::BitmanipOps::ANDN   : result = a_uns & ~b_uns; break;
                case rv::BitmanipOps::MIN    : result = (a_signed < b_signed) ? a_signed : b_signed; break; 
                case rv::BitmanipOps::MINU   : result = (a_uns < b_uns) ? a_uns : b_uns; break; 
                case rv::BitmanipOps::MAX    : result = (a_signed > b_signed) ? a_signed : b_signed; break;
                case rv::BitmanipOps::MAXU   : result = (a_uns > b_uns) ? a_uns : b_uns; break;
                case rv::BitmanipOps::SH1ADD : result = b_uns + (a_uns << 1); break;
                case rv::BitmanipOps::SH2ADD : result = b_uns + (a_uns << 2); break;
                case rv::BitmanipOps::SH3ADD : result = b_uns + (a_uns << 3); break;
            }

            expected_regs[dest] = result;
        }
    }

  protected:
    std::vector<uint32_t> program;
    std::vector<uint32_t> expected_regs;
    std::uniform_int_distribution<uint32_t> reg_gen;
    std::mt19937 gen;

    static const std::vector<uint32_t> BASIC_IMM;
    static const std::vector<uint32_t> BASIC_RR;
    static const std::vector<uint32_t> BITMANIP_IMM;
    static const std::vector<uint32_t> BITMANIP_RR;
    static const std::vector<uint32_t> BITMANIP_UNARY;
};

const std::vector<uint32_t> RandomArithmetic::BASIC_IMM = {
    rv::BaseOps::ADDI,
    rv::BaseOps::SLTI,
    rv::BaseOps::SLTIU,
    rv::BaseOps::XORI,
    rv::BaseOps::ORI,
    rv::BaseOps::ANDI,
    rv::BaseOps::SLLI,
    rv::BaseOps::SRLI,
    rv::BaseOps::SRAI
};

const std::vector<uint32_t> RandomArithmetic::BASIC_RR = {
    rv::BaseOps::ADD,
    rv::BaseOps::SUB,
    rv::BaseOps::SLT,
    rv::BaseOps::SLTU,
    rv::BaseOps::XOR,
    rv::BaseOps::OR,
    rv::BaseOps::AND,
    rv::BaseOps::SLL,
    rv::BaseOps::SRL,
    rv::BaseOps::SRA
};

const std::vector<uint32_t> RandomArithmetic::BITMANIP_IMM = {
    rv::BitmanipOps::RORI,
    rv::BitmanipOps::BEXTI,
    rv::BitmanipOps::BCLRI,
    rv::BitmanipOps::BINVI,
    rv::BitmanipOps::BSETI
};

const std::vector<uint32_t> RandomArithmetic::BITMANIP_UNARY = {
    rv::BitmanipOps::CLZ,
    rv::BitmanipOps::CPOP,
    rv::BitmanipOps::CTZ,
    rv::BitmanipOps::ORCB,
    rv::BitmanipOps::REV8,
    rv::BitmanipOps::SEXTB,
    rv::BitmanipOps::SEXTH,
    rv::BitmanipOps::ZEXTH
};

const std::vector<uint32_t> RandomArithmetic::BITMANIP_RR = {
    rv::BitmanipOps::ROR,
    rv::BitmanipOps::ROL,
    rv::BitmanipOps::BEXT,
    rv::BitmanipOps::BCLR,
    rv::BitmanipOps::BINV,
    rv::BitmanipOps::BSET,
    rv::BitmanipOps::XNOR,
    rv::BitmanipOps::ORN,
    rv::BitmanipOps::ANDN,
    rv::BitmanipOps::MIN,
    rv::BitmanipOps::MINU,
    rv::BitmanipOps::MAX,
    rv::BitmanipOps::MAXU,
    rv::BitmanipOps::SH1ADD,
    rv::BitmanipOps::SH2ADD,
    rv::BitmanipOps::SH3ADD
};

TEST_F(RandomArithmetic, RegisterInit) {
    MakeProgramRegs();
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, ImmediateOps) {
    MakeProgramRegs();
    for (int i = 0; i < 10000; i++) {
        RandomBasicImm();
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

// RegisterOps is a short test to keep registers nonzero
TEST_F(RandomArithmetic, RegisterOps) {
    MakeProgramRegs();
    for (int i = 0; i < 500; i++) {
        RandomBasicReg();
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, BasicOps) {
    MakeProgramRegs();
    for (int i = 0; i < 30000; i++) {
        if (gen() & 1) {
            RandomBasicReg();
        } else {
            RandomBasicImm();
        }
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, BitmanipImmediate) {
    MakeProgramRegs();
    for (int i = 0; i < 100; i++) {
        RandomBitmanipImm();
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, AllImmediateOps) {
    std::uniform_int_distribution<uint32_t> set_choice(0, 1);

    MakeProgramRegs();
    for (int i = 0; i < 10000; i++) {
        switch (set_choice(gen)) {
            case 0 : RandomBasicImm(); break;
            case 1 : RandomBitmanipImm(); break;
        }
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, BitmanipUnaryOps) {
    MakeProgramRegs();
    for (int i = 0; i < 100; i++) {
        RandomBitmanipUnary();
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, AllUnaryOps) {
    std::uniform_int_distribution<uint32_t> set_choice(0, 2);

    MakeProgramRegs();
    for (int i = 0; i < 30000; i++) {
        switch (set_choice(gen)) {
            case 0 : RandomBasicImm(); break;
            case 1 : RandomBitmanipImm(); break;
            case 2 : RandomBitmanipUnary(); break;
        }
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, BitmanipRegOps) {
    MakeProgramRegs();
    for (int i = 0; i < 500; i++) {
        RandomBitmanipReg();
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, RegOps) {
    MakeProgramRegs();
    for (int i = 0; i < 500; i++) {
        if (gen() & 1) {
            RandomBasicReg();
        } else {
            RandomBitmanipReg();
        }
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}

TEST_F(RandomArithmetic, AllArithmeticOps) {
    std::uniform_int_distribution<uint32_t> set_choice(0, 4);

    MakeProgramRegs();
    for (int i = 0; i < 50000; i++) {
        switch (set_choice(gen)) {
            case 0 : RandomBasicImm(); break;
            case 1 : RandomBasicReg(); break;
            case 2 : RandomBitmanipImm(); break;
            case 3 : RandomBitmanipUnary(); break;
            case 4 : RandomBitmanipReg(); break;
        }
    }
    MakeProgramEnd();

    RunProgram();

    Check();
}
