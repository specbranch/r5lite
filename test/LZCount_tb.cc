// LZCount testbench
//
// Perform leading zero counts on random numbers and check against Intel/AMD
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#include <cstdlib>
#include <iostream>

#include "VLZCount.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

#define TICK \
    contextp->timeInc(1); \
    top->eval(); \
    tracer->dump(contextp->time())

int main(int argc, char** argv) {
    // Setup context
    auto contextp = std::make_unique<VerilatedContext>();
    contextp->debug(0);
    contextp->randReset(2);
    contextp->traceEverOn(true);
    contextp->commandArgs(argc, argv);

    // Get the top module
    auto top = std::make_unique<VLZCount>(contextp.get(), "TOP");

    // Start tracing
    std::string trace_filename = std::string(argv[0]) + ".vcd";
    auto tracer = std::make_unique<VerilatedVcdC>();
    top->trace(tracer.get(), 99);
    tracer->open(trace_filename.c_str());

    int bit_errors [32] = {0};
    int errors = 0;

    std::cout << "Testing 10000 leading zero counts..." << std::endl;

    // Check the module
    for (int trial = 0; trial < 10000; trial++) {
        uint32_t data = rand();
        uint32_t data_shift = rand() % 32;
        data >>= data_shift;

        top->in = data;
        TICK;
        uint32_t expected = (data == 0) ? 32 : __builtin_clz(data);

        if (expected != top->out) {
            std::cout << "ERROR: Expected leading 0 count of " << expected
                      << " for input 0x" << std::hex << data << std::dec
                      << " but got " << (unsigned int) top->out << std::endl;

            ++errors;
            for (int i = 0; i < 32; i++) {
                if (data & (1 << i)) {
                    ++bit_errors[i];
                }
            }
        }
    }

    if (errors == 0) {
        std::cout << "Passed!" << std::endl;
    } else {
        std::cout << "Bit errors: " << std::endl;
        for (int i = 0; i < 32; i++) {
            std::cout << "    " << i << " : " << bit_errors[i] << std::endl;
        }
    }
    return 0;
}