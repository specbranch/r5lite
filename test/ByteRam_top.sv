// Top module to pass useful parameters to the ByteRam module for simulation
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module ByteRam_top (
    input  wire         read_en,
    input  wire         write_en,
    input  wire  [11:0] address,
    input  wire  [31:0] write_data,
    input  wire  [3:0]  write_mask,

    output logic        read_ready,
    output logic [31:0] read_data,

    input  wire         clk,
    input  wire         reset
);

    R5L_ByteRam #(.ADDR_WIDTH (12), .DATA_BYTES (4)) dut (
        .read_en,
        .write_en,
        .address,
        .write_data,
        .write_mask,
        .read_ready,
        .read_data,
        .clk,
        .reset
    );

endmodule