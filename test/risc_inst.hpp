// C++ Header containing the properties of RISC-V instructions
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#include <cstdint>

namespace rv {

static constexpr uint32_t DEST = 7;
static constexpr uint32_t OPERAND_A = 15;
static constexpr uint32_t OPERAND_B = 20;
static constexpr uint32_t FUNCT3 = 12;
static constexpr uint32_t FUNCT7 = 25;
static constexpr uint32_t S_IMMEDIATE = 20;

enum Opcode : uint32_t {
    OPCODE_LOAD      = 0b0000011,
    OPCODE_LOAD_FP   = 0b0000111,
    OPCODE_MISC_MEM  = 0b0001111,
    OPCODE_OP_IMM    = 0b0010011,
    OPCODE_AUIPC     = 0b0010111,
    OPCODE_OP_IMM_32 = 0b0011011,
    OPCODE_STORE     = 0b0100011,
    OPCODE_STORE_FP  = 0b0100111,
    OPCODE_AMO       = 0b0101111,
    OPCODE_OP        = 0b0110011,
    OPCODE_LUI       = 0b0110111,
    OPCODE_OP_32     = 0b0111011,
    OPCODE_MADD      = 0b1000011,
    OPCODE_MSUB      = 0b1000111,
    OPCODE_NMSUB     = 0b1001011,
    OPCODE_NMADD     = 0b1001111,
    OPCODE_OP_FP     = 0b1010011,
    OPCODE_BRANCH    = 0b1100011,
    OPCODE_JALR      = 0b1100111,
    OPCODE_JAL       = 0b1101111,
    OPCODE_SYSTEM    = 0b1110011
};

static constexpr uint32_t OPCODE_MASK = (1 << 7) - 1;

enum BaseOps : uint32_t {
    LUI    = Opcode::OPCODE_LUI,
    AUIPC  = Opcode::OPCODE_AUIPC,
    JAL    = Opcode::OPCODE_JAL,
    JALR   = Opcode::OPCODE_JALR,
    FENCE  = Opcode::OPCODE_MISC_MEM,
    ECALL  = Opcode::OPCODE_SYSTEM,
    EBREAK = Opcode::OPCODE_SYSTEM | (1 << S_IMMEDIATE),

    BEQ    = Opcode::OPCODE_BRANCH,
    BNE    = Opcode::OPCODE_BRANCH | (1 << FUNCT3),
    BLT    = Opcode::OPCODE_BRANCH | (4 << FUNCT3),
    BGE    = Opcode::OPCODE_BRANCH | (5 << FUNCT3),
    BLTU   = Opcode::OPCODE_BRANCH | (6 << FUNCT3),
    BGEU   = Opcode::OPCODE_BRANCH | (7 << FUNCT3),

    LB     = Opcode::OPCODE_LOAD,
    LH     = Opcode::OPCODE_LOAD   | (1 << FUNCT3),
    LW     = Opcode::OPCODE_LOAD   | (2 << FUNCT3),
    LBU    = Opcode::OPCODE_LOAD   | (4 << FUNCT3),
    LHU    = Opcode::OPCODE_LOAD   | (5 << FUNCT3),

    SB     = Opcode::OPCODE_STORE,
    SH     = Opcode::OPCODE_STORE  | (1 << FUNCT3),
    SW     = Opcode::OPCODE_STORE  | (2 << FUNCT3),

    ADDI   = Opcode::OPCODE_OP_IMM,
    SLTI   = Opcode::OPCODE_OP_IMM | (2 << FUNCT3),
    SLTIU  = Opcode::OPCODE_OP_IMM | (3 << FUNCT3),
    XORI   = Opcode::OPCODE_OP_IMM | (4 << FUNCT3),
    ORI    = Opcode::OPCODE_OP_IMM | (6 << FUNCT3),
    ANDI   = Opcode::OPCODE_OP_IMM | (7 << FUNCT3),
    SLLI   = Opcode::OPCODE_OP_IMM | (1 << FUNCT3),
    SRLI   = Opcode::OPCODE_OP_IMM | (5 << FUNCT3),
    SRAI   = Opcode::OPCODE_OP_IMM | (5 << FUNCT3) | (0b0100000 << FUNCT7),

    ADD    = Opcode::OPCODE_OP,
    SUB    = Opcode::OPCODE_OP                     | (0b0100000 << FUNCT7),
    SLT    = Opcode::OPCODE_OP     | (2 << FUNCT3),
    SLTU   = Opcode::OPCODE_OP     | (3 << FUNCT3),
    XOR    = Opcode::OPCODE_OP     | (4 << FUNCT3),
    OR     = Opcode::OPCODE_OP     | (6 << FUNCT3),
    AND    = Opcode::OPCODE_OP     | (7 << FUNCT3),
    SLL    = Opcode::OPCODE_OP     | (1 << FUNCT3),
    SRL    = Opcode::OPCODE_OP     | (5 << FUNCT3),
    SRA    = Opcode::OPCODE_OP     | (5 << FUNCT3) | (0b0100000 << FUNCT7)
};

enum MulOps : uint32_t {
    MUL    = Opcode::OPCODE_OP                 | (1 << FUNCT7),
    MULH   = Opcode::OPCODE_OP | (1 << FUNCT3) | (1 << FUNCT7),
    MULHSU = Opcode::OPCODE_OP | (2 << FUNCT3) | (1 << FUNCT7),
    MULHU  = Opcode::OPCODE_OP | (3 << FUNCT3) | (1 << FUNCT7),
    DIV    = Opcode::OPCODE_OP | (4 << FUNCT3) | (1 << FUNCT7),
    DIVU   = Opcode::OPCODE_OP | (5 << FUNCT3) | (1 << FUNCT7),
    REM    = Opcode::OPCODE_OP | (6 << FUNCT3) | (1 << FUNCT7),
    REMU   = Opcode::OPCODE_OP | (7 << FUNCT3) | (1 << FUNCT7)
};

enum BitmanipOps : uint32_t {
    ROL    = Opcode::OPCODE_OP     | (1 << FUNCT3) | (0b0110000 << FUNCT7),
    ROR    = Opcode::OPCODE_OP     | (5 << FUNCT3) | (0b0110000 << FUNCT7),
    RORI   = Opcode::OPCODE_OP_IMM | (5 << FUNCT3) | (0b0110000 << FUNCT7),

    BEXTI  = Opcode::OPCODE_OP_IMM | (5 << FUNCT3) | (0b0100100 << FUNCT7),
    BCLRI  = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b0100100 << FUNCT7),
    BINVI  = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b0110100 << FUNCT7),
    BSETI  = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b0010100 << FUNCT7),
    BEXT   = Opcode::OPCODE_OP     | (5 << FUNCT3) | (0b0100100 << FUNCT7),
    BCLR   = Opcode::OPCODE_OP     | (1 << FUNCT3) | (0b0100100 << FUNCT7),
    BINV   = Opcode::OPCODE_OP     | (1 << FUNCT3) | (0b0110100 << FUNCT7),
    BSET   = Opcode::OPCODE_OP     | (1 << FUNCT3) | (0b0010100 << FUNCT7),

    CLZ    = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b011000000000 << S_IMMEDIATE),
    CPOP   = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b011000000010 << S_IMMEDIATE),
    CTZ    = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b011000000001 << S_IMMEDIATE),
    ORCB   = Opcode::OPCODE_OP_IMM | (5 << FUNCT3) | (0b001010000111 << S_IMMEDIATE),
    REV8   = Opcode::OPCODE_OP_IMM | (5 << FUNCT3) | (0b011010011000 << S_IMMEDIATE),
    SEXTB  = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b011000000100 << S_IMMEDIATE),
    SEXTH  = Opcode::OPCODE_OP_IMM | (1 << FUNCT3) | (0b011000000101 << S_IMMEDIATE),
    ZEXTH  = Opcode::OPCODE_OP     | (4 << FUNCT3) | (0b000010000000 << S_IMMEDIATE),

    XNOR   = Opcode::OPCODE_OP     | (4 << FUNCT3) | (0b0100000 << FUNCT7),
    ORN    = Opcode::OPCODE_OP     | (6 << FUNCT3) | (0b0100000 << FUNCT7),
    ANDN   = Opcode::OPCODE_OP     | (7 << FUNCT3) | (0b0100000 << FUNCT7),

    MIN    = Opcode::OPCODE_OP     | (4 << FUNCT3) | (0b0000101 << FUNCT7),
    MINU   = Opcode::OPCODE_OP     | (5 << FUNCT3) | (0b0000101 << FUNCT7),
    MAX    = Opcode::OPCODE_OP     | (6 << FUNCT3) | (0b0000101 << FUNCT7),
    MAXU   = Opcode::OPCODE_OP     | (7 << FUNCT3) | (0b0000101 << FUNCT7),

    SH1ADD = Opcode::OPCODE_OP     | (2 << FUNCT3) | (0b0010000 << FUNCT7),
    SH2ADD = Opcode::OPCODE_OP     | (4 << FUNCT3) | (0b0010000 << FUNCT7),
    SH3ADD = Opcode::OPCODE_OP     | (6 << FUNCT3) | (0b0010000 << FUNCT7)
};

uint32_t ImmediateBits (uint32_t instruction) {
    switch(instruction & OPCODE_MASK) {
        case Opcode::OPCODE_JAL    : return 20;
        case Opcode::OPCODE_AUIPC  : return 20;
        case Opcode::OPCODE_LUI    : return 20;
        case Opcode::OPCODE_LOAD   : return 12;
        case Opcode::OPCODE_STORE  : return 12;
        case Opcode::OPCODE_JALR   : return 12;
        case Opcode::OPCODE_BRANCH : return 12;
        case Opcode::OPCODE_OP_IMM : {
            if (((instruction >> FUNCT3) & 0x7) == 0x1) {
                switch (instruction >> S_IMMEDIATE) {
                    case 0b011000000000 :
                    case 0b011000000001 :
                    case 0b011000000100 :
                    case 0b011000000101 :
                    case 0b011000000010 : return 0;
                    default             : return 5;
                }
            }
            if (((instruction >> FUNCT3) & 0x7) == 0x5) {
                switch (instruction >> S_IMMEDIATE) {
                    case 0b001010000111 :
                    case 0b011010011000 : return 0;
                    default             : return 5;
                }
            }
            return 12;
        };
        default : return 0;
    }
}

uint32_t MakeImmediate (uint32_t immediate, uint32_t instruction) {
    switch(instruction & OPCODE_MASK) {
        case Opcode::OPCODE_JAL : {
            uint32_t top_bit = immediate >> 19;
            uint32_t middle_bit = (immediate >> 10) & 1;
            uint32_t top_section = (immediate >> 12) & ((1 << 8) - 1);
            uint32_t bottom_section = immediate & ((1 << 10) - 1);

            instruction |= top_section << 12;
            instruction |= middle_bit << 20;
            instruction |= bottom_section << 21;
            instruction |= top_bit << 31;
            return instruction;
        } break;
        case Opcode::OPCODE_BRANCH : {
            uint32_t top_bit = immediate >> 11;
            uint32_t next_bit = (immediate >> 10) & 1;
            uint32_t top_section = (immediate >> 5) & ((1 << 6) - 1);
            uint32_t bottom_section = immediate & ((1 << 4) - 1);

            instruction |= next_bit << 7;
            instruction |= bottom_section << 8;
            instruction |= top_section << 25;
            instruction |= top_bit << 31;
            return instruction;
        } break;
        case Opcode::OPCODE_STORE : {
            uint32_t immediate_bottom = immediate & ((1 << 5) - 1);
            uint32_t immediate_top = immediate >> 5;
            return instruction | (immediate_top << 25) | (immediate_bottom << 7);
        } break;
        case Opcode::OPCODE_JALR :
        case Opcode::OPCODE_LOAD :
        case Opcode::OPCODE_OP_IMM : {
            return instruction | (immediate << 20);
        } break;
        case Opcode::OPCODE_AUIPC :
        case Opcode::OPCODE_LUI : {
            return instruction | (immediate << 12);
        } break;
        default : return instruction;
    }
}

}