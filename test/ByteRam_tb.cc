// ByteRam testbench
//
// Use random transactions against a model to verify
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#include <cstdlib>
#include <iostream>
#include <vector>

#include "VByteRam_top.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

#define TICK \
    contextp->timeInc(1); \
    top->clk = !top->clk; \
    top->eval(); \
    tracer->dump(contextp->time()); \
    contextp->timeInc(1); \
    top->clk = !top->clk; \
    top->eval(); \
    tracer->dump(contextp->time())

int main(int argc, char** argv) {
    constexpr int DATA_BYTES = 1 << 12;
    constexpr int DATA_WORDS = 1 << 10;

    // Model of a byte-oriented RAM
    std::vector<uint8_t> data;
    data.resize(DATA_BYTES, 0);

    // Setup context
    auto contextp = std::make_unique<VerilatedContext>();
    contextp->debug(0);
    contextp->randReset(2);
    contextp->traceEverOn(true);
    contextp->commandArgs(argc, argv);

    // Get the top module
    auto top = std::make_unique<VByteRam_top>(contextp.get(), "TOP");

    // Start tracing
    std::string trace_filename = std::string(argv[0]) + ".vcd";
    auto tracer = std::make_unique<VerilatedVcdC>();
    top->trace(tracer.get(), 99);
    tracer->open(trace_filename.c_str());

    top->clk = 1;
    top->reset = 1;
    for (int i = 0; i < 20; i++) {
        TICK;
    }
    top->reset = 0;
    top->read_en = 0;

    // Clear the memory to get to a known state
    top->write_en = 1;
    top->write_mask = 0xf;
    for (int i = 0; i < DATA_WORDS; i++) {
        top->address = i * 4;
        top->write_data = 0;
        TICK;
    }
    top->write_en = 0;

    std::cout << "Testing 10000 random reads and writes" << std::endl;

    // Do a bunch of random writes and reads
    for (int i = 0; i < 10000; i++) {
        top->write_en = 0;
        top->read_en = 0;

        uint32_t write_data = rand();
        uint32_t address = rand() % DATA_BYTES;
        while (address > DATA_BYTES - 4) {
            address = rand() % DATA_BYTES;
        }
        uint8_t mask = rand() & 0xf;

        top->address = address;

        int mode = rand() % 4;
        if (mode & 1) {
            top->write_en = 1;
            top->write_data = write_data;
            top->write_mask = mask;
        } else {
            top->write_data = rand();
        }
        if (mode & 2) {
            top->read_en = 1;
        }

        TICK;

        // Check read data
        if (mode & 2) {
            if (top->read_ready != 1) {
                std::cout << "ERROR: Expected read return, did not get one!"
                          << std::endl;
            }
            uint32_t read_data = top->read_data;
            uint32_t ref_data = *reinterpret_cast<uint32_t*>(data.data() + address);

            if (read_data != ref_data) {
                std::cout << "ERROR: Read " << std::hex << read_data
                          << " but expected " << ref_data << std::endl;
            }
        } else {
            if (top->read_ready != 0) {
                std::cout << "ERROR: Got extra read return!" << std::endl;
            }
        }

        // Apply the write
        if (mode & 1) {
            for (int i = 0; i < 4; i++) {
                if (mask & (1 << i)) {
                    *(data.data() + address + i) = *(reinterpret_cast<uint8_t*>(&write_data) + i);
                }
            }
        }
    }

    top->write_en = 0;

    // Do a final memory scan
    top->read_en = 1;
    for (int i = 0; i < DATA_WORDS; i++) {
        top->address = i * 4;
        top->write_data = rand();
        TICK;
        if (top->read_ready != 1) {
            std::cout << "ERROR: Expected read return, did not get one!"
                        << std::endl;
        }
        uint32_t read_data = top->read_data;
        uint32_t ref_data = *(reinterpret_cast<uint32_t*>(data.data()) + i);
        if (read_data != ref_data) {
            std::cout << "ERROR: Read " << std::hex << read_data
                        << " but expected " << ref_data << std::endl;
        }
    }
    top->read_en = 0;

    std::cout << "Simulation completed" << std::endl;

    return 0;
}