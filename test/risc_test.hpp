// R5Lite Core Testbench
//
// The test harness used for all of the full-core tests.  It provides a simple platform for
// abstracting the actual function of Verilator away from the code that just intends to test
// functionality.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#pragma once

#include <gtest/gtest.h>

#include <cstdint>
#include <cstdlib>
#include <chrono>
#include <functional>
#include <map>
#include <memory>
#include <random>
#include <sstream>
#include <vector>

#include "VR5LiteCore.h"
#include "VR5LiteCore_R5LiteCore.h"
#include "VR5LiteCore_R5L_RegFile.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

class RiscTest : public ::testing::Test {
  protected:
    void SetUp() override {
        contextp = std::make_unique<VerilatedContext>();
        contextp->debug(0);
        contextp->randReset(2);
        contextp->traceEverOn(true);

        top = std::make_unique<VR5LiteCore>(contextp.get(), "TOP");
        trace = std::make_unique<VerilatedVcdC>();

        std::stringstream tracename;
        tracename << ::testing::UnitTest::GetInstance()->current_test_info()->name() << ".vcd";
        top->trace(trace.get(), 99);
        trace->open(tracename.str().c_str());

        top->inst_ready = 0;
        top->data_ready = 0;
        top->data_wdone = 0;
        top->start = 0;
        top->clk = 1;
        top->reset = 1;
        top->pause = 0;
        for (int i = 0; i < 20; i++) {
            Tick();
        }
        top->reset = 0;
        Tick();
    }

    void InitializeRegs(const std::map<uint32_t, uint32_t>& reg_values) {
        auto& reg_file = top->R5LiteCore->regFile->registers;

        for (auto& reg_val : reg_values) {
            reg_file[reg_val.first] = reg_val.second;
        }
        Tick();
    }

    void InitializeMemory(std::vector<uint8_t>&& memory) {
        byte_ram = std::move(memory);
    }

    void Run(const std::vector<uint32_t>& program, std::size_t mem_size, int expected_time, bool strict_timeout = true) {
        top->start;
        int cycles = 0;

        std::hash<int64_t> hash;
        std::mt19937_64 gen(hash(std::chrono::steady_clock::now().time_since_epoch().count()));

        // Resize the RAM and fill randomly if the memory hasn't been initialized
        if (byte_ram.size() == 0 && mem_size != 0) {
            byte_ram.resize(mem_size + 3);
            for (auto& element : byte_ram) {
                element = gen();
            }
        }

        top->start = 1;
        Tick();
        top->start = 0;
        Fall();

        // Run the code
        while (top->running) {
            // Sample at the rising edge
            bool inst_read = top->inst_read_en;
            uint32_t inst_addr = top->inst_addr;
            bool data_read = top->data_read_en;
            uint32_t data_addr = top->data_addr;
            bool data_write = top->data_write_en;

            // Handle writes
            if (top->data_write_en) {
                for (int i = 0; i < 4; i++) {
                    if (top->data_wmask & (1 << i)) {
                        ASSERT_LT(top->data_addr + i, byte_ram.size());
                        uint8_t* src = reinterpret_cast<uint8_t*>(&top->data_wdata) + i;
                        *(byte_ram.data() + top->data_addr + i) = *(src);
                    }
                }
            }

            Rise();

            // Handle instruction read
            if (inst_read) {
                ASSERT_EQ((inst_addr & 0x3), 0);
                uint32_t idx = inst_addr >> 2;
                ASSERT_LT(idx, program.size());
                top->inst_rdata = program[idx];
                top->inst_ready = 1;
            } else {
                top->inst_rdata = gen();
                top->inst_ready = 0;
            }

            // Handle data memory read and write
            if (data_read) {
                ASSERT_LT(data_addr, byte_ram.size());
                top->data_rdata = *reinterpret_cast<uint32_t*>(byte_ram.data() + data_addr);
                top->data_ready = 1;
            } else {
                top->data_rdata = gen();
                top->data_ready = 0;
            }

            if (data_write) {
                ASSERT_LT(data_addr, byte_ram.size());
                top->data_wdone = 1;
            } else {
                top->data_wdone = 0;
            }

            // Run a cycle of simulation
            Fall();

            ++cycles;
            ASSERT_LE(cycles, expected_time);
        }

        if (strict_timeout) {
            EXPECT_EQ(cycles, expected_time);
        }

        Rise();

        // Drain the pipeline
        top->inst_ready = 0;
        top->data_ready = 0;
        Tick();
        Tick();
        Tick();
    }

    void CheckRam(const std::vector<uint8_t>& expected, int start_address = 0) {
        for (int i = start_address; i < expected.size(); i++) {
            EXPECT_EQ(byte_ram[i], expected[i]);
        }
    }

    void CheckRegs(const std::vector<uint32_t>& expected) {
        auto& reg_file = top->R5LiteCore->regFile->registers;

        for (int i = 1; i < expected.size(); i++) {
            EXPECT_EQ(reg_file[i], expected[i]);
        }
    }
    void CheckRegs(const std::map<uint32_t, uint32_t>& expected) {
        auto& reg_file = top->R5LiteCore->regFile->registers;

        for (auto& reg : expected) {
            EXPECT_EQ(reg_file[reg.first], reg.second);
        }
    }

  private:
    void Fall() {
        contextp->timeInc(1);
        top->clk = 0;
        top->eval();
        trace->dump(contextp->time());
    }
    void Rise() {
        contextp->timeInc(1);
        top->clk = 1;
        top->eval();
        trace->dump(contextp->time());
    }
    void Tick() {
        Fall();
        Rise();
    }

    std::unique_ptr<VerilatedContext> contextp;
    std::unique_ptr<VR5LiteCore> top;
    std::unique_ptr<VerilatedVcdC> trace;

    std::vector<uint8_t> byte_ram;
};