// Basic tests of the R5Lite Core, including most of the weird corner cases in the branch predictor
// and some small toy programs to test simple functionality.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#include <gtest/gtest.h>

#include <cstdint>
#include <cstdlib>
#include <vector>

#include "risc_test.hpp"

// Test the ADDI instruction and the bypassing paths
TEST_F(RiscTest, AddiTest) {
    std::vector<uint32_t> program = {
        0x3e800093,     // ADDI X1, X0, 1000
        0x7d008113,     // ADDI X2, X1, 2000
        0xc1810193,     // ADDI X3, x2, -1000
        0x83018213,     // ADDI X4, X3, -2000
        0x3e820293,     // ADDI X5, X4, 1000
        0x00100073,     // EBREAK
        0x08008093      // ADDI X1, X1, 0x080    should not execute
    };
    std::vector<uint32_t> regs = {0, 1000, 3000, 2000, 0, 1000};
    Run(program, 0, program.size());
    CheckRegs(regs);
}

TEST_F(RiscTest, LuiTest) {
    std::vector<uint32_t> program = {
        0x010200b7,
        0x30408093,
        0xdeadf137,
        0x800001b7,
        0x00d10213,
        0x3e800293,
        0xfff18193,
        0x111112b7,
        0x00100073,    // EBREAK
        0x08008093     // ADDI X1, X1, 0x080    should not execute
    };
    std::vector<uint32_t> regs = {0, 0x01020304, 0xdeadf000, 0x7fffffff, 0xdeadf00d, 0x11111000};
    Run(program, 0, program.size());
    CheckRegs(regs);
}

TEST_F(RiscTest, ImmediateOps) {
    std::vector<uint32_t> program = {
        0x808080b7,    // LUI X1, 0x80808
        0x4040e113,    // ORI X2, X1, 0x404
        0x08008093,    // ADDI X1, X1, 0x080
        0xff017193,    // ANDI X3, X2, 0xFFFFFFF0
        0x0990c213,    // XORI X4, X1, 0x099
        0x00421293,    // SLLI X5, X4, 4
        0x4082d313,    // SRAI X6, X5, 8
        0x00125393,    // SRLI X7, X4, 1
        0x0223c413,    // XORI X8, X7, 0x022
        0xfff0c493,    // XORI X9, X1, -1
        0x01b4d493,    // SRLI X9, X9, 27
        0xfff4c493,    // XORI X9, X9, -1
        0x0054a513,    // SLTI X10, X9, 5
        0x0144b593,    // SLTIU X11, X9, 20
        0x00100073,    // EBREAK
        0x08008093,    // ADDI X1, X1, 0x080    should not execute
        0x08008093,    // ADDI X1, X1, 0x080    should not execute
        0x08008093     // ADDI X1, X1, 0x080    should not execute
    };

    Run(program, 0, 16);

    std::vector<uint32_t> regs;
    regs.resize(12, 0);

    regs[1] = 0x80808000;
    regs[2] = regs[1] | 0x404;
    regs[1] = regs[1] + 0x080;
    regs[3] = regs[2] & 0xfffffff0;
    regs[4] = regs[1] ^ 0x099;
    regs[5] = regs[4] << 4;
    regs[6] = static_cast<uint32_t>(static_cast<int32_t>(regs[5]) >> 8);
    regs[7] = regs[4] >> 1;
    regs[8] = regs[7] ^ 0x022;
    regs[9] = ~((~regs[1]) >> 27);
    regs[10] = 1;
    regs[11] = 0;

    CheckRegs(regs);
}

TEST_F(RiscTest, RegisterOps) {
    std::vector<uint32_t> program = {
        0x3e800093,     // ADDI X1, X0, 1000
        0x7d008113,     // ADDI X2, X1, 2000
        0x001101b3,     // ADD X3, X2, X1
        0x00116233,     // OR X4, X2, X1
        0x001172b3,     // AND X5, X2, X1
        0x00514333,     // XOR X6, X2, X5
        0x00600393,     // ADDI X7, X0, 6
        0x00709633,     // SLL X12, X1, X7
        0x40238433,     // SUB X8, X7, X2
        0x002434b3,     // SLTU X9, X8, X2
        0x00942533,     // SLT X10, X8, X9
        0x407455b3,     // SRA X11, X8, X7
        0x0071d6b3,     // SRL X13, X3, X7
        0x00100073,     // EBREAK
        0x00000013      // NOP
    };

    Run(program, 0, program.size());

    std::vector<uint32_t> regs;
    regs.resize(14, 0);
    regs[1] = 1000;
    regs[2] = 3000;
    regs[3] = regs[1] + regs[2];
    regs[4] = regs[1] | regs[2];
    regs[5] = regs[1] & regs[2];
    regs[6] = regs[2] ^ regs[5];
    regs[7] = 6;
    regs[8] = regs[7] - regs[2];
    regs[9] = 0;
    regs[10] = 1;
    regs[11] = (static_cast<int>(regs[8]) >> 6);
    regs[12] = regs[1] << 6;
    regs[13] = regs[3] >> 6;

    CheckRegs(regs);
}

// TODO: Figure out how to test bitmanip instructions

TEST_F(RiscTest, PCToRegs) {
    std::vector<uint32_t> program = {
        0xfefff097,    // AUIPC X1, 0xfefff
        0x01000117,    // AUIPC X2, 0x01000
        0x00400297,    // AUIPC X5, 0x00400
        0x40510333,    // SUB X6, X2, X5
        0x001101b3,    // ADD X3, X2, X1
        0x00308233,    // ADD X4, X1, X3
        0x006303b3,    // ADD X7, X6, X6
        0x00100073,    // EBREAK
        0x08008093     // ADDI X1, X1, 0x080    should not execute
    };

    Run(program, 0, program.size());

    std::vector<uint32_t> regs;
    regs.resize(8, 0);
    regs[1] = 0xfefff000;
    regs[2] = 0x01000004;
    regs[5] = 0x00400008;
    regs[3] = regs[2] + regs[1];
    regs[4] = regs[1] + regs[3];
    regs[6] = regs[2] - regs[5];
    regs[7] = regs[6] + regs[6];

    CheckRegs(regs);
}

TEST_F(RiscTest, SimpleJumpAndLink) {
    std::vector<uint32_t> program = {
        0x3e800093,    // ADDI X1, X0, 1000
        0x0140016f,    // JAL X2, +20
        0x002083b3,    // ADD X7, X1, X2
        0x0013e3b3,    // OR X7, X7, X1
        0x00028367,    // JALR X6, X5
        0x00100073,    // EBREAK (program end)
        0x7d010193,    // ADDI X3, X2, 2000
        0x0011f233,    // AND X4, X3, X1
        0x000102e7,    // JALR X5, X2
        0x00030467,    // JALR X8, X6
        0x808080b7,    // LUI X1, 0x80808 (never executed)
        0x808080b7     // LUI X1, 0x80808 (never executed)
    };

    // 14 cycles = 1 for spinup, 6 for 3 JALRs (2 each), and 7 for other instructions
    Run(program, 0, 14);

    std::vector<uint32_t> regs;
    regs.resize(9, 0);
    regs[1] = 1000;
    regs[2] = 8;
    regs[3] = regs[2] + 2000;
    regs[4] = regs[3] & regs[1];
    regs[5] = 0x24;
    regs[6] = 0x14;
    regs[8] = 0x28;
    regs[7] = regs[1] + regs[2];
    regs[7] = regs[7] | regs[1];

    CheckRegs(regs);
}

TEST_F(RiscTest, SmallLoops) {
    std::vector<uint32_t> program = {
        0x06400093,     // ADDI X1, X0, 100
        0x00a00113,     // ADDI X2, X0, 10
        0x0031c1b3,     // XOR X3, X3, X3
        // LOOP1:
        0x001181b3,     // ADD X3, X3, X1
        0xfff08093,     // ADDI X1, X1, -1
        0xfff10113,     // ADDI X2, X2, -1
        0xfe011ae3,     // BNE X2, X0, LOOP1
        // END LOOP 1
        0x00a00213,     // ADDI X4, X0, 10
        // LOOP2:
        0x001181b3,     // ADD X3, X3, X1
        0xfff08093,     // ADDI X1, X1, -1
        0x00415663,     // BGE X2, X4, EXIT
        0x00110113,     // ADDI X2, X2, 1
        0xff1ff06f,     // JAL X0, LOOP2
        // END LOOP 2
        // EXIT:
        0x00100073,     // EBREAK
        0x08008093      // ADDI X1, X1, 0x080    should not execute
    };

    // The first loop runs 10 times (4 cycles each)
    // The second loop runs 11 times, 10 times taking 5 cycles, and the last taking 3
    // Each incurs one misprediction, plus we have 5 other instructions and spin-up time
    int cycles = 6 + (10 * 4) + 2 + (10 * 5 + 3);
    Run(program, 0, cycles);

    std::vector<uint32_t> regs;
    regs.resize(5, 0);
    regs[1] = 79;
    regs[2] = 10;
    regs[4] = 10;

    regs[3] = 0;
    for (uint32_t i = 80; i <= 100; i++) {
        regs[3] += i;
    }

    CheckRegs(regs);
}

TEST_F(RiscTest, UnsignedBranches) {
    std::vector<uint32_t> program = {
        // Start
        0x100001b7,     // LUI X3, 0x10000
        0x0a000237,     // LUI X4, 0x0a000
        0x003002b7,     // LUI X5, 0x00300
        0x00020337,     // LUI X6, 0x00020
        // Reset:
        0xc942b0b7,     // LUI X1, 0xc942b
        0x2a708093,     // ADDI X1, X1, 0x2a7
        0x0100016f,     // JAL X2, Switch
        0xff5ff16f,     // JAL X2, Reset - Never Executed
        0x00100073,     // EBREAK
        // Loop:
        0x0010d093,     // SRLI X1, X1, 1
        // Switch:
        0xfc008ce3,     // BEQ X1, X0, Start
        0xfe30fce3,     // BGEU X1, X3, Loop
        0x0040d093,     // SRLI X1, X1, 4
        0x0040e463,     // BLTU X1, X4, Skip
        0x00209093,     // SLLI X1, X1, 2
        // Skip:
        0xfe60e4e3,     // BLTU X1, X6, Loop
        0xfe50f2e3,     // BGEU X1, X5, Loop
        0x00410067,     // JALR X0, X2, 4
        0x08008093      // ADDI X1, X1, 0x080    should not execute
    };

    // The constant 49 was determined from simulation
    Run(program, 0, 49);

    std::vector<uint32_t> regs = {0, 412181, 28, 0x10000000, 0x0a000000, 0x00300000, 0x00020000};

    CheckRegs(regs);
}

TEST_F(RiscTest, EqualLoop) {
    std::vector<uint32_t> program = {
        0xff600093,     // ADDI X1, X0, -10
        0x00a00113,     // ADDI X2, X0, 10
        0x00217213,     // ANDI X4, X2, 2
        0x0100006f,     // JAL X0, Trampoline
        0x00100073,     // EBREAK
        0x00108093,     // ADDI X1, X1, 1
        // Bad:
        0x00220233,     // ADD X4, X4, X2
        // Trampoline:
        0x00000197,     // AUIPC X3, 0
        0x0080006f,     // JAL X0, LOOP
        // Return:
        0xff418067,     // JALR X0, X3, -12
        // Loop:
        0x40410133,     // SUB X2, X2, X4
        0xfe110ce3,     // BEQ X2, X1, Return
        0x00115463,     // BGE X2, X1, Continue
        0xfe5ff26f,     // JAL X4, Bad
        // Continue:
        0xff1ff06f      // JAL X0, Loop
    };

    // The constant 66 was determined from simulation
    // We expect to loop 10 times, with 9 cycles of overhead and about 6 cycles/loop
    Run(program, 0, 66);

    std::vector<uint32_t> regs = {0, 0xfffffff6, 0xfffffff6, 0x1c, 2};

    CheckRegs(regs);
}

TEST_F(RiscTest, JalrVaryingAddress) {
    std::vector<uint32_t> program = {
        0x00000093,     // ADDI X1, X0, 0
        0x00400113,     // ADDI X2, X0, 4
        0x034001ef,     // JAL X3, End
        0xffc28213,     // ADDI X4, X5, -4
        0x00000197,     // AUIPC X3, 0
        0x003101b3,     // ADD X3, X2, X3
        0x00818193,     // ADDI X3, X3, 8
        // Loop
        0x40220233,     // SUB X4, X4, X2
        0x00020067,     // JALR X0, X4
        // Instructions hit by JALRs
        0x00100073,     // EBREAK
        0x00108093,     // ADDI X1, X1, 1
        0x402080b3,     // SUB X1, X1, X2
        0x0020a0b3,     // SLT X1, X1, X2
        0x001150b3,     // SRL X1, X2, X1
        0x002090b3,     // SLL X1, X1, X2
        // End:
        0x000182e7,     // JALR X5, X3
        0x08008093      // ADDI X1, X1, 0x080    should not execute
    };

    // We expect to loop 7 times, with cycle count growing each loop
    // from 6 cycles on the first iteration to 10 cycles, total 5+6+7+8+9+10+4 = 49
    // Plus 9 cycles of setup
    Run(program, 0, 54);

    std::vector<uint32_t> regs = {0, 0x40, 4, 0x1c, 0x24, 0x40};
    CheckRegs(regs);
}

TEST_F(RiscTest, SimpleByteMemory) {
    std::vector<uint32_t> program = {
        0x40000093,     // ADDI X1, X0, 0x400
        0x50000113,     // ADDI X2, X0, 0x500
        // StoreLoop:
        0xc0108023,     // SB X1, -1024(X1)
        0x00108093,     // ADDI X1, X1, 1
        0xfe20ece3,     // BLTU X1, X2, StoreLoop
        0x00000193,     // ADDI X3, X0, 0
        0x00018293,     // ADDI X5, X3, 0
        0x00028313,     // ADDI X6, X5, 0
        0x10000213,     // ADDI X4, X0, 0x100
        // LoadLoop:
        0x00018083,     // LB X1, (X3)
        0x001282b3,     // ADD X5, X5, X1
        0x0001c083,     // LBU X1, (X3)
        0x00608333,     // ADD X6, X1, X6
        0x00118193,     // ADDI X3, X3, 1
        0xfe41c6e3,     // BLT X3, X4, LoadLoop
        0x00100073,     // EBREAK
        0x00000013      // ADDI X0, X0, 0
    };

    Run(program, 256, 3 * 256 + 10 * 256 + 10);

    std::vector<uint32_t> regs = {0, 0xff, 0x500, 0x100, 0x100, static_cast<uint32_t>(-128), 255 * 128};
    CheckRegs(regs);

    std::vector<uint8_t> memory;
    for (int i = 0; i < 0x100; i++) {
        memory.emplace_back(i);
    }
    CheckRam(memory);
}

TEST_F(RiscTest, SimpleHalfMemory) {
    std::vector<uint32_t> program = {
        0xffff00b7,     // LUI X1, 0xffff0
        0x00108093,     // ADDI X1, X1, 1
        0x00008113,     // ADDI X2, X1, 0
        0x00000193,     // ADDI X3, X0, 0
        0x04000213,     // ADDI X4, X0, 0x40
        0x000182b3,     // ADD X5, X3, X0
        0x40518333,     // SUB X6, X3, X5
        // StoreLoop:
        0x0411a023,     // SW X1, 64(X3)
        0x00418193,     // ADDI X3, X3, 4
        0x001100b3,     // ADD X1, X2, X1
        0xfe41cae3,     // BLT X3, X4, StoreLoop
        0x003201b3,     // ADD X3, X4, X3
        // LoadLoop:
        0x00021083,     // LH X1, (X4)
        0x005082b3,     // ADD X5, X1, X5
        0x00025103,     // LHU X2, (X4)
        0x00220213,     // ADDI X4, X4, 2
        0x00230333,     // ADD X6, X6, X2
        0xfe3266e3,     // BLTU X4, X3, LoadLoop
        0x00100073,     // EBREAK
        0x00000013      // ADDI X0, X0, 0
    };

    Run(program, 0x80, 10 + 4 * 16 + 2 + 9 * 32);

    uint32_t sum = 0;
    for (int i = 1; i <= 16; i++) {
        sum += i;
        sum += 0x10000 - i;
    }

    std::vector<uint32_t> regs = {0, 0xfffffff0, 0xfff0, 0x80, 0x80, 0, sum};
    CheckRegs(regs);

    std::vector<uint8_t> memory;
    memory.resize(0x80);
    uint32_t* words = reinterpret_cast<uint32_t*>(memory.data() + 0x40);
    uint32_t increment = 0xffff0001;
    uint32_t word = 0xffff0001;
    for (int i = 0; i < 0x10; i++) {
        words[i] = word;
        word += increment;
    }
    CheckRam(memory, 0x40);
}

TEST_F(RiscTest, SimpleWordMemory) {
    std::vector<uint32_t> program = {
        0x00000093,     // ADDI X1, X0, 0
        0x00000113,     // ADDI X2, X0, 0
        0x00000193,     // ADDI X3, X0, 0
        0x40000213,     // ADDI X4, X0, 0x400
        // StoreLoop:
        0x0011a023,     // SW X1, (X3)
        0x00418193,     // ADDI X3, X3, 4
        0x00108093,     // ADDI X1, X1, 1
        0xfe41eae3,     // BLTU X3, X4, StoreLoop
        0xffb18193,     // ADDI X3, X3, -5
        // LoadLoop:
        0xfff1a083,     // LW X1, -1(X3)
        0xffc18193,     // ADDI X3, X3, -4
        0x00208133,     // ADD X2, X1, X2
        0xfe01dae3,     // BGE X3, X0, LoadLoop
        0x00100073,     // EBREAK
        0x00000013      // ADDI X0, X0, 0
    };

    Run(program, 0x400, 9 + 4 * 0x100 + 5 * 0xff);

    uint32_t sum = 0;
    for (int i = 0; i < 0x100; i++) {
        sum += 0x10000 * i;
    }

    std::vector<uint32_t> regs = {0, 0x10000, sum, 0xffffffff, 0x400};
    CheckRegs(regs);

    std::vector<uint8_t> memory;
    memory.resize(0x400);
    uint32_t* words = reinterpret_cast<uint32_t*>(memory.data());
    for (int i = 0; i < 0x100; i++) {
        words[i] = i;
    }
    CheckRam(memory);
}

// Bubble sorting tests
TEST_F(RiscTest, BubbleSortReversed) {
    // Bubble sort program
    std::vector<uint32_t> program = {
        0x00200613,
        0x06c5c063,
        0xfff58813,
        0x00450893,
        0x00c0006f,
        0x0015f593,
        0x04058663,
        0x00052603,
        0x00000593,
        0x00088713,
        0x00080793,
        0x01c0006f,
        0xfed72e23,
        0x00c72023,
        0x00100593,
        0xfff78793,
        0x00470713,
        0xfc0788e3,
        0x00072683,
        0xfec6c2e3,
        0x00068613,
        0xfff78793,
        0x00470713,
        0xfe0796e3,
        0xfb5ff06f,
        0x00100073,
        0x00000013
    };

    int size = 33;

    std::vector<int> to_sort;
    for (int i = 0; i < size; i++) {
        to_sort.emplace_back(size - i);
    }

    std::vector<uint8_t> memory;
    memory.resize(size * 4);
    std::memcpy(memory.data(), to_sort.data(), size * 4);

    InitializeMemory(std::move(memory));

    std::map<uint32_t, uint32_t> reg_map;
    reg_map.emplace(10, 0);
    reg_map.emplace(11, size);
    InitializeRegs(reg_map);

    Run(program, size * 4, 100000, false);

    // Figure out the expected memory post-sorting and check
    std::sort(to_sort.begin(), to_sort.end());
    memory.resize(size * 4);
    std::memcpy(memory.data(), to_sort.data(), size * 4);
    CheckRam(memory);
}
TEST_F(RiscTest, BubbleSortOrdered) {
    // Bubble sort program
    std::vector<uint32_t> program = {
        0x00200613,
        0x06c5c063,
        0xfff58813,
        0x00450893,
        0x00c0006f,
        0x0015f593,
        0x04058663,
        0x00052603,
        0x00000593,
        0x00088713,
        0x00080793,
        0x01c0006f,
        0xfed72e23,
        0x00c72023,
        0x00100593,
        0xfff78793,
        0x00470713,
        0xfc0788e3,
        0x00072683,
        0xfec6c2e3,
        0x00068613,
        0xfff78793,
        0x00470713,
        0xfe0796e3,
        0xfb5ff06f,
        0x00100073,
        0x00000013
    };

    int size = 97;

    std::vector<int> to_sort;
    for (int i = 0; i < size; i++) {
        to_sort.emplace_back(i);
    }

    std::vector<uint8_t> memory;
    memory.resize(size * 4);
    std::memcpy(memory.data(), to_sort.data(), size * 4);

    InitializeMemory(std::move(memory));

    std::map<uint32_t, uint32_t> reg_map;
    reg_map.emplace(10, 0);
    reg_map.emplace(11, size);
    InitializeRegs(reg_map);

    Run(program, size * 4, 1000, false);

    // Figure out the expected memory post-sorting and check
    std::sort(to_sort.begin(), to_sort.end());
    memory.resize(size * 4);
    std::memcpy(memory.data(), to_sort.data(), size * 4);
    CheckRam(memory);
}
TEST_F(RiscTest, BubbleCalled) {
    // Bubble sort program
    std::vector<uint32_t> program = {
        // Caller
        0x00000097,
        0x00c080e7,
        0x00100073,

        // Bubble sort program
        0x00200613,
        0x06c5c063,
        0xfff58813,
        0x00450893,
        0x00c0006f,
        0x0015f593,
        0x04058663,
        0x00052603,
        0x00000593,
        0x00088713,
        0x00080793,
        0x01c0006f,
        0xfed72e23,
        0x00c72023,
        0x00100593,
        0xfff78793,
        0x00470713,
        0xfc0788e3,
        0x00072683,
        0xfec6c2e3,
        0x00068613,
        0xfff78793,
        0x00470713,
        0xfe0796e3,
        0xfb5ff06f,
        0x00008067,
        0xfec6c2e3  // Random instruction that is never executed (padding)
    };

    int size = 97;

    std::vector<int> to_sort;
    for (int i = 0; i < size; i++) {
        to_sort.emplace_back(i);
    }

    std::vector<uint8_t> memory;
    memory.resize(size * 4);
    std::memcpy(memory.data(), to_sort.data(), size * 4);

    InitializeMemory(std::move(memory));

    std::map<uint32_t, uint32_t> reg_map;
    reg_map.emplace(10, 0);
    reg_map.emplace(11, size);
    InitializeRegs(reg_map);

    Run(program, size * 4, 1000, false);

    // Figure out the expected memory post-sorting and check
    std::sort(to_sort.begin(), to_sort.end());
    memory.resize(size * 4);
    std::memcpy(memory.data(), to_sort.data(), size * 4);
    CheckRam(memory);
}

// Recursion: Recursive fibonacci
TEST_F(RiscTest, RecursiveFibonacci) {
    std::vector<uint32_t> program = {
        // Caller
        0x00000097,
        0x00c080e7,
        0x00100073,

        // Fibonacci(int)->int
        0xff010113,
        0x00112623,
        0x00812423,
        0x00912223,
        0x01212023,
        0x00200593,
        0x00050413,
        0x00000493,
        0x02b56063,
        0x00100913,
        0xfff40513,
        0x00000097,
        0xfd4080e7,
        0xffe40413,
        0x00a484b3,
        0xfe8966e3,
        0x00940533,
        0x00c12083,
        0x00812403,
        0x00412483,
        0x00012903,
        0x01010113,
        0x00008067,
        0xfec6c2e3  // Random instruction that is never executed (padding)
    };

    uint32_t index = 13;
    uint32_t expected_out = 233;

    // Pre-initialize the argument and the stack pointer
    std::map<uint32_t, uint32_t> reg_map;
    reg_map.emplace(2, 1024);
    reg_map.emplace(10, index);
    InitializeRegs(reg_map);

    Run(program, 1024, 50000, false);

    // Check the stack pointer and the result
    std::map<uint32_t, uint32_t> expected;
    expected.emplace(2, 1024);
    expected.emplace(10, expected_out);
    CheckRegs(expected);
}

// Memory: Array fibonacci
TEST_F(RiscTest, ArrayFibonacci) {
    std::vector<uint32_t> program = {
        // Caller
        0x00000097,
        0x00c080e7,
        0x00100073,

        // Fibonacci(short)->short
        0xe0010113,
        0x00011023,
        0x00100593,
        0x00200613,
        0x00b11123,
        0x02c54863,
        0x00150613,
        0x01061613,
        0x01065693,
        0x00410613,
        0xffe68693,
        0xffc61703,
        0x00e585b3,
        0x00b61023,
        0xfff68693,
        0x00260613,
        0xfe0696e3,
        0x00151513,
        0x00010593,
        0x00b50533,
        0x00051503,
        0x20010113,
        0x00008067,
        0xfec6c2e3  // Random instruction that is never executed (padding)
    };

    uint32_t index = 23;
    uint32_t expected_out = 28657;

    // Pre-initialize the argument and the stack pointer
    std::map<uint32_t, uint32_t> reg_map;
    reg_map.emplace(2, 1024);
    reg_map.emplace(10, index);
    InitializeRegs(reg_map);

    Run(program, 1024, 1000, false);

    // Check the stack pointer and the result
    std::map<uint32_t, uint32_t> expected;
    expected.emplace(2, 1024);
    expected.emplace(10, expected_out);
    CheckRegs(expected);
}

TEST_F(RiscTest, BranchyCompiledFunction) {
    std::vector<uint32_t> program = {
        // Caller
        0x00000097,
        0x00c080e7,
        0x00100073,

        // Function:
        //  a in [0, 101] -> a + 2
        //  a in [102, 256] -> a + 1
        //  a in [257, 512] -> a - 2
        //  a in [513, 1024] -> a - 1
        //  a in [1025, 65536] -> 2
        //  a > (1 << 24) -> a + 3
        //  a in [(1 << 20) + 1, (1 << 24)] -> a + 4
        //  else -> a - 3
        // Compiled without optimization
        0xfe010113,
        0x00112e23,
        0x00812c23,
        0x02010413,
        0xfea42823,
        0xff245503,
        0x00000593,
        0x08b51a63,
        0x0040006f,
        0xff042583,
        0x3ff00513,
        0x06b56a63,
        0x0040006f,
        0xff042503,
        0x20100593,
        0x00b56a63,
        0x0040006f,
        0xfff00513,
        0xfea42623,
        0x0500006f,
        0xff042503,
        0x10100593,
        0x00b56a63,
        0x0040006f,
        0xffe00513,
        0xfea42623,
        0x0300006f,
        0xff042503,
        0x06600593,
        0x00b56a63,
        0x0040006f,
        0x00100513,
        0xfea42623,
        0x0100006f,
        0x00200513,
        0xfea42623,
        0x0040006f,
        0x0040006f,
        0x0040006f,
        0x0100006f,
        0x00200513,
        0xfea42a23,
        0x0700006f,
        0x0580006f,
        0xff042503,
        0x010005b7,
        0x00158593,
        0x00b56a63,
        0x0040006f,
        0x00300513,
        0xfea42623,
        0x0340006f,
        0xff042503,
        0x001005b7,
        0x00158593,
        0x00b56a63,
        0x0040006f,
        0x00400513,
        0xfea42623,
        0x0100006f,
        0xffd00513,
        0xfea42623,
        0x0040006f,
        0x0040006f,
        0x0040006f,
        0xff042503,
        0xfec42583,
        0x00b50533,
        0xfea42a23,
        0x0040006f,
        0xff442503,
        0x01c12083,
        0x01812403,
        0x02010113,
        0x00008067,
        0xe0010113  // Padding
    };

    uint32_t a = 102;
    uint32_t expected_out = 103;

    // Pre-initialize the argument and the stack pointer
    std::map<uint32_t, uint32_t> reg_map;
    reg_map.emplace(2, 1024);
    reg_map.emplace(10, a);
    InitializeRegs(reg_map);

    Run(program, 1024, 100, false);

    // Check the stack pointer and the result
    std::map<uint32_t, uint32_t> expected;
    expected.emplace(2, 1024);
    expected.emplace(10, expected_out);
    CheckRegs(expected);
}
