// Flushable FIFO testbench
//
// Use random transactions to verify and compare against a model
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <random>
#include <vector>

#include "VFlushableFifo.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

#define TICK \
    contextp->timeInc(1); \
    top->clk = !top->clk; \
    top->eval(); \
    tracer->dump(contextp->time())

int main(int argc, char** argv) {
    // Setup context
    auto contextp = std::make_unique<VerilatedContext>();
    contextp->debug(0);
    contextp->randReset(2);
    contextp->traceEverOn(true);
    contextp->commandArgs(argc, argv);

    // Get the top module
    auto top = std::make_unique<VFlushableFifo>(contextp.get(), "TOP");

    // Start tracing
    std::string trace_filename = std::string(argv[0]) + ".vcd";
    auto tracer = std::make_unique<VerilatedVcdC>();
    top->trace(tracer.get(), 99);
    tracer->open(trace_filename.c_str());

    int errors = 0;

    std::vector<uint32_t> addresses;
    std::vector<uint32_t> data;
    std::size_t read_pointer = 0;

    std::deque<std::size_t> write_times;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::bernoulli_distribution ack_dist(0.5);
    std::bernoulli_distribution addr_dist(0.3);
    std::uniform_int_distribution<std::size_t> delay_dist(1, 8);
    std::bernoulli_distribution flush_dist(0.003);

    top->clk = 1;
    top->reset = 1;
    top->write_data = 0;
    top->write_addr = 0;
    top->flush = 0;
    top->read_ack = 0;
    for (int i = 0; i < 20; i++) {
        TICK;
    }
    top->reset = 0;

    constexpr std::size_t TRIALS = 5000;
    constexpr std::size_t TIMEOUT = 30000;
    std::size_t cycle = 0;

    std::cout << "Testing " << TRIALS << " transactions to the FIFO..." << std::endl;

    while (read_pointer < TRIALS && cycle < TIMEOUT) {
        bool send_address = (addresses.size() < TRIALS) && !top->full && addr_dist(gen);
        bool ack_data = ack_dist(gen);
        bool flush_time = flush_dist(gen);

        // Write commands and data
        top->addr = gen();
        top->data = gen();

        // Flush first, because we only want to flush PAST writes, not the write this cycle
        if (flush_time) {
            top->flush = 1;

            // Advance the next thing we expect to read
            read_pointer = addresses.size();
        } else {
            top->flush = 0;
        }

        if (send_address) {
            addresses.emplace_back(top->addr);
            write_times.emplace_back(cycle + delay_dist(gen));
            top->write_addr = 1;
        } else {
            top->write_addr = 0;
        }

        // Write the data after the pre-determined time
        if (!write_times.empty() && cycle >= write_times.front()) {
            write_times.pop_front();
            data.emplace_back(top->data);
            top->write_data = 1;
        } else {
            top->write_data = 0;
        }

        // Write ack
        top->read_ack = ack_data ? 1 : 0;

        TICK;

        // Read data
        if (top->read_ack && top->read_valid) {
            uint32_t read_data = top->read_data;
            uint32_t read_addr = top->read_addr;

            if (read_pointer >= data.size()) {
                std::cout << "FATAL: Reader is ahead of writer!" << std::endl;
                return 1;
            }

            if (read_data != data[read_pointer]) {
                ++errors;
                std::cout << "ERROR: [" << std::dec << cycle
                            << "] Got wrong data!     Got " << std::hex << read_data
                            << " but expected " << data[read_pointer] << std::endl;
            }
            if (read_addr != addresses[read_pointer]) {
                ++errors;
                std::cout << "ERROR: [" << std::dec << cycle
                            << "] Got wrong address!  Got " << std::hex << read_addr
                            << " but expected " << addresses[read_pointer] << std::endl;
            }
            ++read_pointer;
        }

        TICK;
        ++cycle;
    }

    if (read_pointer != TRIALS) {
        std::cout << "ERROR: Did not get " << TRIALS << " reads" << std::endl;
        return 1;
    }

    if (errors > 0) {
        std::cout << "ERROR: Had " << std::dec << errors << " during the test!" << std::endl;
        return 1;
    } else {
        std::cout << "Completed successfully!" << std::endl;
    }

    return 0;
}