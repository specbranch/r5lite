RM := rm -rf
MV := mv
CC := g++
VERILATOR := verilator --cc --exe --build -O3 --x-assign unique --x-initial unique --trace
LD_GTEST := -LDFLAGS "-lgtest -lgtest_main"

CORE_SRCS := rtl/Internal_pkg.sv rtl/RVInst_pkg.sv rtl/Shifter.sv rtl/Compressors.sv rtl/LZCount.sv \
			 rtl/Popcount.sv rtl/FlushableFifo.sv rtl/RegFile.sv rtl/Backend.sv rtl/Decode.sv rtl/Fetch.sv \
			 rtl/R5LiteCore.sv
CORE_TOP := --top-module R5LiteCore

CORE_TESTS := basic_test random_arithmetic_test
UNIT_TESTS := byteram_test lzcount_test popcount_test flushable_fifo_test
TARGETS := $(CORE_TESTS) $(UNIT_TESTS)

# VCD filenames from gtest tests, which generate one VCD file for each test
VCDS := AddiTest AllArithmeticOps AllImmediateOps AllUnaryOps ArrayFibonacci BasicOps BitmanipImmediate \
		BitmanipRegOps BitmanipUnaryOps BranchyCompiledFunction BubbleCalled BubbleSortOrdered \
		BubbleSortReversed  EqualLoop ImmediateOps JalrVaryingAddress LuiTest PCToRegs RecursiveFibonacci \
		RegisterInit RegisterOps RegOps SimpleByteMemory SimpleHalfMemory SimpleJumpAndLink SimpleWordMemory \
		SmallLoops UnsignedBranches		

###################################################################################################

all: $(TARGETS)

clean:
	$(RM) obj_dir/
	$(RM) $(TARGETS)
	$(RM) $(foreach wave, $(UNIT_TESTS), $(wave).vcd)
	$(RM) $(foreach wave, $(VCDS), $(wave).vcd)

###################################################################################################

# Unit tests of components
byteram_test: rtl/ByteRam.sv rtl/Shifter.sv test/ByteRam_top.sv test/ByteRam_tb.cc
	$(VERILATOR) $^ -o $@ --top-module ByteRam_top
	$(MV) obj_dir/$@ .
	@echo ""
	./$@
	@echo ""

lzcount_test: rtl/LZCount.sv test/LZCount_tb.cc
	$(VERILATOR) $^ -o $@
	$(MV) obj_dir/$@ .
	@echo ""
	./$@
	@echo ""

popcount_test: rtl/Compressors.sv rtl/Popcount.sv test/Popcount_tb.cc
	$(VERILATOR) $^ -o $@ --top-module R5L_Popcount
	$(MV) obj_dir/$@ .
	@echo ""
	./$@
	@echo ""

flushable_fifo_test: rtl/FlushableFifo.sv test/FlushableFifo_tb.cc
	$(VERILATOR) $^ -o $@
	$(MV) obj_dir/$@ .
	@echo ""
	./$@
	@echo ""

###################################################################################################

# Integration tests of the core
basic_test: $(CORE_SRCS) test/basic_tests.cc
	$(VERILATOR) $^ -o $@ $(CORE_TOP) $(LD_GTEST)
	$(MV) obj_dir/$@ .
	@echo ""
	./$@
	@echo ""

random_arithmetic_test: $(CORE_SRCS) test/random_arithmetic.cc
	$(VERILATOR) $^ -o $@ $(CORE_TOP) $(LD_GTEST)
	$(MV) obj_dir/$@ .
	@echo ""
	./$@
	@echo ""

