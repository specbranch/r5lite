// FWFT Flushable FIFO using a small RAM for aligning addresses and data on a memory bus
// Assumes that data always arrives at least 1 cycle after the address
//
// Flush empties the FIFO of address slots, allowing data to lag behind, but the FIFO will
// be seen as "empty" until the data catches up with the new addresses written after the flush
//
// For FWFT and minimal latency, we have a combinational path from data to read_data, and a
// guard against advancing the read pointer when the FIFO is empty.  This means that a reader
// can assert read constantly when it is ready for new data, and will get it.  Additionally,
// data has a 0-cycle latency to become available to the next stage.
//
// Sits between the fetch and decode stages.  Only needs to be large enough to cover
// instruction read latency (2 slots in a BRAM-backed system, no more than ~32 slots in
// general).
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_FlushableFifo #(
    parameter int DATA_WIDTH = 32,
    parameter int ADDR_WIDTH = 32,
    parameter int PTR_WIDTH = 4
) (
    input  wire  [DATA_WIDTH - 1 : 0] data,
    input  wire                       write_data,

    input  wire  [ADDR_WIDTH - 1 : 0] addr,
    input  wire                       write_addr,

    output logic [DATA_WIDTH - 1 : 0] read_data,
    output logic [ADDR_WIDTH - 1 : 0] read_addr,
    output logic                      read_valid,
    input  wire                       read_ack,

    output logic                      full,
    input  wire                       flush,

    input  wire                       reset,
    input  wire                       clk
);

    logic [PTR_WIDTH - 1 : 0] read_ptr, flush_ptr, addr_write_ptr, data_write_ptr;

    // Read pointer can "snap" to the write pointer on flush
    always_ff @(posedge clk) begin
        if (reset) read_ptr <= '0;
        else begin
            if (flush)                       read_ptr <= addr_write_ptr;
            else if (read_ack && read_valid) read_ptr <= read_ptr + 1'b1;
        end
    end

    // Store the last place we flushed
    always_ff @(posedge clk) begin
        if (flush) flush_ptr <= addr_write_ptr - 1'b1;
    end

    // Write pointers are normal
    logic [PTR_WIDTH - 1 : 0] next_addr_write_ptr, next_data_write_ptr;
    always_comb begin
        next_addr_write_ptr = addr_write_ptr + ((write_addr & ~full) ? PTR_WIDTH'(1) : PTR_WIDTH'(0));
        next_data_write_ptr = data_write_ptr + (write_data ? PTR_WIDTH'(1) : PTR_WIDTH'(0));
    end
    always_ff @(posedge clk) begin
        if (reset) begin
            addr_write_ptr <= '0;
            data_write_ptr <= '0;
        end
        else begin
            addr_write_ptr <= next_addr_write_ptr;
            data_write_ptr <= next_data_write_ptr;
        end
    end

    logic [ADDR_WIDTH - 1 : 0] addr_ram [2 ** PTR_WIDTH];
    logic [DATA_WIDTH - 1 : 0] data_ram [2 ** PTR_WIDTH];

    always_ff @(posedge clk) begin
        if (write_data) data_ram[data_write_ptr] <= data;
        if (write_addr) addr_ram[addr_write_ptr] <= addr;
    end

    logic data_behind;
    always_ff @(posedge clk) begin
        if (reset) data_behind <= '0;
        else begin
            if (flush && (next_data_write_ptr != addr_write_ptr)) data_behind <= '1;
            else if (write_data && (data_write_ptr == flush_ptr)) data_behind <= '0;
        end
    end

    // Combinational RAM read (assuming a small RAM) with a bypass path on data
    // Allows 0 cycle latency from incoming data, but requires addresses to be at least
    // 1 cycle old - this matches with most memory bus systems in architecture
    always_comb begin
        read_data = (read_ptr == data_write_ptr) ? data : data_ram[read_ptr];
        read_addr = addr_ram[read_ptr];
    end

    always_comb begin
        read_valid = !flush && !data_behind && ((read_ptr != data_write_ptr) || write_data);
        full  = addr_write_ptr == (read_ptr - 1'b1);
    end

endmodule
