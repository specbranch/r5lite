// Top module for an R5Lite Core, which uses a read-only instruction bus and a read-write memory bus
// to interact with the outside world.  The memory bus is desigend for a ByteRam or an adapter to
// a different protocol.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5LiteCore (
    // Byte-oriented data memory port
    output logic [31:0] data_addr,
    output logic        data_write_en,
    output logic        data_read_en,
    output logic [31:0] data_wdata,
    output logic [3:0]  data_wmask,
    input  wire         data_ready,
    input  wire  [31:0] data_rdata,
    input  wire         data_wdone,

    // Read-only instruction port
    output logic [31:0] inst_addr,
    output logic        inst_read_en,
    input  wire         inst_ready,
    input  wire  [31:0] inst_rdata,

    // Status signals
    output logic running,
    input  wire  start,
    input  wire  pause,

    // Global signals
    input  wire reset,
    input  wire clk
);

    import R5L_Internal_pkg::*;

    // Run state of the CPU
    logic trap;
    logic run_state;
    always_ff @(posedge clk) begin
        if (reset) run_state <= '0;
        else begin
            if (trap)       run_state <= '0;
            else if (start) run_state <= '1;
        end
    end
    always_comb running = run_state & ~pause;

    // Intermediate signals between frontend and backend
    r5l_micro_op_t micro_op;
    logic          uop_valid;
    logic          backend_ack;

    logic [31:0]   writeback_data;
    logic [4:0]    writeback_addr;
    logic          writeback_en;
    logic          writeback_is_read;
    memory_width_t writeback_width;
    logic          writeback_signed;

    logic          mispredict_valid;
    logic [31:0]   mispredict_pc;
    logic          predict_valid;
    logic [31:0]   predict_pc;

    logic [31:0]   decode_pc;
    logic [31:0]   decode_inst;
    logic          decode_valid;
    logic          decode_ack;

    logic [4:0]    read_a_addr;
    logic [4:0]    read_b_addr;
    logic [31:0]   read_a_data;
    logic [31:0]   read_b_data;

    // Phase 1: fetch the instruction
    R5L_Fetch fetch (
        .inst_addr,
        .inst_read_en,
        .inst_ready,
        .inst_rdata,

        .decode_pc,
        .decode_inst,
        .decode_valid,
        .decode_ack,

        .predict_pc,
        .predict_valid,
        .mispredict_pc,
        .mispredict_valid,

        .run (running),
        .reset,
        .clk
    );

    // Phase 2: decode into microcode
    R5L_Decode decoder (
        .decode_pc,
        .decode_inst,
        .decode_valid,
        .decode_ack,

        .backend_ack,
        .uop_valid,
        .micro_op,

        .read_a_addr,
        .read_a_data,
        .read_b_addr,
        .read_b_data,

        .predict_pc,
        .predict_valid,
        .mispredicted (mispredict_valid),
        .load_ended   (data_ready),
        .store_ended  (data_wdone),

        .trap,
        .run          (running),
        .reset,
        .clk
    );

    // Phase 3: execute the instruction
    R5L_Backend backend (
        .backend_ack,
        .uop_valid,
        .micro_op,

        .mispredict_pc,
        .mispredict_valid,

        .data_addr,
        .data_write_en,
        .data_read_en,
        .data_wdata,
        .data_wmask,

        .writeback_data,
        .writeback_addr,
        .writeback_en,
        .writeback_is_read,
        .writeback_width,
        .writeback_signed,

        .reset,
        .clk
    );

    // Register file
    R5L_RegFile regFile (
        .writeback_data,
        .writeback_addr,
        .writeback_en,
        .writeback_is_read,
        .writeback_width,
        .writeback_signed,

        .read_return_data  (data_rdata),
        .read_return_ready (data_ready),

        .read_a_addr,
        .read_a_data,
        .read_b_addr,
        .read_b_data,

        .reset,
        .clk
    );

endmodule
