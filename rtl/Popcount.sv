// Popcount module based on 4:2 compressors and full adders, specifically for 32-bit words
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_Popcount (
    input  wire  [31:0] in,
    output logic [5:0]  out
);

    // First compressor layer
    logic [10:0] first_layer_ones;
    logic [10:0] first_layer_twos;

    generate
        genvar m;
        for (m = 0; m < 11; m += 1) begin : gen_first_layer
            localparam int LSB = m * 3;
            localparam int MSB = (LSB + 2 > 31) ? 31 : LSB + 2;
            R5L_FullAdder firstLayer (
                /* verilator lint_off WIDTH */
                .in  (in[MSB:LSB]),
                /* verilator lint_on WIDTH */
                .out ({first_layer_twos[m], first_layer_ones[m]})
            );
        end
    endgenerate

    // Second compressor layer: we have 11 2-bit numbers
    // First layer reduction: 2 4:2 compressors configured with 5 inputs
    // Second layer reduction: 2 4:2 compressors and a 3:2
    logic [2:0] second_layer_ones;
    logic [4:0] second_layer_twos;
    logic [4:0] second_layer_fours;

    // Passed inputs
    always_comb second_layer_ones[2] = first_layer_ones[10];

    logic second_layer_carries [2];
    generate
        genvar n;
        for (n = 0; n < 2; n += 1) begin : gen_second_layer_ones
            R5L_FourCompressor onesPlace (
                .cin  (first_layer_ones[n * 5 + 4]),
                .cout (second_layer_carries[n]),
                .bits (first_layer_ones[n * 5 + 3 : n * 5]),
                .out  ({second_layer_twos[n], second_layer_ones[n]})
            );
        end

        for (n = 0; n < 3; n += 1) begin : gen_second_layer_twos
            localparam int LSB = n * 4;
            localparam int MSB = (LSB + 3 > 10) ? 10 : LSB + 3;

            if (MSB - LSB < 3) begin
                R5L_FullAdder twosPlace (
                    /* verilator lint_off WIDTH */
                    .in  (first_layer_twos[MSB:LSB]),
                    /* verilator lint_on WIDTH */
                    .out ({second_layer_fours[n], second_layer_twos[n + 2]})
                );
            end
            else begin
                R5L_FourCompressor twosPlace (
                    .cin  (second_layer_carries[n]),
                    .cout (second_layer_fours[n + 3]),
                    .bits (first_layer_twos[MSB:LSB]),
                    .out ({second_layer_fours[n], second_layer_twos[n + 2]})
                );
            end
        end
    endgenerate

    // Third layer: a pair of compressors on twos and fours
    logic [2:0] third_layer_ones;
    logic [2:0] third_layer_twos;
    logic [1:0] third_layer_fours;
    logic [1:0] third_layer_eights;

    R5L_FullAdder twoReduction (
        .in  (second_layer_twos[4:2]),
        .out ({third_layer_fours[1], third_layer_twos[2]})
    );
    R5L_FourCompressor fourReduction (
        .cin  (second_layer_fours[4]),
        .bits (second_layer_fours[3:0]),
        .out  ({third_layer_eights[0], third_layer_fours[0]}),
        .cout (third_layer_eights[1])
    );

    // Passed between rounds
    always_comb begin
        third_layer_ones      = second_layer_ones;
        third_layer_twos[1:0] = second_layer_twos[1:0];
    end

    // Final ternary adder
    logic [5:0] final_add [3];
    always_comb begin
        final_add[0] = {2'd0, third_layer_eights[0], third_layer_fours[0], third_layer_twos[0], third_layer_ones[0]};
        final_add[1] = {2'd0, third_layer_eights[1], third_layer_fours[1], third_layer_twos[1], third_layer_ones[1]};
        final_add[2] = {4'd0, third_layer_twos[2], third_layer_ones[2]};
    end
    always_comb out = final_add[0] + final_add[1] + final_add[2];

endmodule
