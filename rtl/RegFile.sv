// Register file for the R5Lite core, featuring 2 write ports and 2 read ports
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_RegFile (
    input  wire  [31:0]                     writeback_data,
    input  wire  [4:0]                      writeback_addr,
    input  wire                             writeback_en,
    input  wire                             writeback_is_read,
    input  R5L_Internal_pkg::memory_width_t writeback_width,
    input  wire                             writeback_signed,

    input  wire  [31:0]                     read_return_data,
    input  wire                             read_return_ready,

    output logic [31:0]                     read_a_data,
    input  wire  [4:0]                      read_a_addr,

    output logic [31:0]                     read_b_data,
    input  wire  [4:0]                      read_b_addr,

    input  wire                             reset,
    input  wire                             clk
);

    import R5L_Internal_pkg::*;

    // Slot for the address of an incoming read return
    logic [4:0]    read_return_addr;
    memory_width_t read_return_width;
    logic          read_return_signed;
    always_ff @(posedge clk) begin
        if (writeback_en && writeback_is_read) begin
            read_return_addr   <= writeback_addr;
            read_return_width  <= writeback_width;
            read_return_signed <= writeback_signed;
        end
    end

    logic [31:0] registers [32] /* verilator public */;

    // Sign extend the loaded data
    logic loaded_data_sign;
    always_comb begin
        case (read_return_width)
            MEMORY_BYTE : loaded_data_sign = read_return_data[7];
            MEMORY_HALF : loaded_data_sign = read_return_data[15];
            default     : loaded_data_sign = 'x;
        endcase
    end
    wire load_extend_bit = read_return_signed ? loaded_data_sign : 1'b0;

    logic [31:0] loaded_data;
    always_comb begin
        case (read_return_width)
            MEMORY_BYTE : loaded_data = {{24{load_extend_bit}}, read_return_data[7:0]};
            MEMORY_HALF : loaded_data = {{16{load_extend_bit}}, read_return_data[15:0]};
            default     : loaded_data = read_return_data;
        endcase
    end

    always_ff @(posedge clk) begin
        if (read_return_ready) begin
            registers[read_return_addr] <= loaded_data;
        end
        if (writeback_en) begin
            registers[writeback_addr] <= writeback_data;
        end
    end

    always_comb begin
        if (read_a_addr == 0) read_a_data = '0;
        else                  read_a_data = registers[read_a_addr];
    end
    always_comb begin
        if (read_b_addr == 0) read_b_data = '0;
        else                  read_b_data = registers[read_b_addr];
    end

endmodule
