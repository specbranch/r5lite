// Backend unit for the R5Lite core
//
// The backend is likely the critical path in most systems.  The backend consists of a shifter and
// an ALU, with the shifter sitting in front of the ALU.  This gives us a very flexible system for
// bitmanip instructions and what we need later for fast divide.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.
//
// TODO: Add multiply and divide

module R5L_Backend (
    output logic [31:0]                     data_addr,
    output logic                            data_write_en,
    output logic                            data_read_en,
    output logic [31:0]                     data_wdata,
    output logic [3:0]                      data_wmask,

    input  R5L_Internal_pkg::r5l_micro_op_t micro_op,
    input  wire                             uop_valid,
    output logic                            backend_ack,    // TODO: determine if we need this

    output logic [31:0]                     writeback_data,
    output logic [4:0]                      writeback_addr,
    output logic                            writeback_en,
    output logic                            writeback_is_read,
    output R5L_Internal_pkg::memory_width_t writeback_width,
    output logic                            writeback_signed,

    output logic                            mispredict_valid,
    output logic [31:0]                     mispredict_pc,

    input  wire                             reset,
    input  wire                             clk
);

    import R5L_Internal_pkg::*;

    always_comb backend_ack = '1;

    wire [31:0] extended_immediate = {{20{micro_op.short_immediate[11]}}, micro_op.short_immediate};

    // BYPASSING
    logic [31:0] bypass_reg, operand_a, operand_b;
    always_comb begin
        if (micro_op.bypass_a) operand_a = bypass_reg;
        else                   operand_a = micro_op.operand_a;
        if (micro_op.bypass_b) operand_b = bypass_reg;
        else                   operand_b = micro_op.operand_b;
    end

    // -----------------------------------------------------------------------------------------------
    // MISC UNARY OPERATIONS
    // Popcount, byte swapping, leading/trailing zero count, bytewise OR-combine
    // TODO: implement byewise OR combining
    wire [31:0] byte_swap = {operand_a[7:0], operand_a[15:8], operand_a[23:16], operand_a[31:24]};

    // Popcount
    logic [5:0] pop_count;
    R5L_Popcount popCounter (
        .in  (operand_a),
        .out (pop_count)
    );

    // Bit reversal to handle leading and trailing zero count in the same circuit
    logic [31:0] zero_count_in;
    always_comb begin
        for (int i = 0; i < 32; i++) begin
            if (micro_op.invert_b) zero_count_in[i] = operand_a[31 - i];
            else                   zero_count_in[i] = operand_a[i];
        end
    end

    logic [5:0] zero_count;
    R5L_LZCount #(.WIDTH(32)) zeroCounter (
        .in  (zero_count_in),
        .out (zero_count)
    );

    wire [31:0] unary_sext_b = {{24{operand_a[7]}}, operand_a[7:0]};
    wire [31:0] unary_sext_h = {{16{operand_a[15]}}, operand_a[15:0]};

    logic [3:0] byte_ors;
    always_comb begin
        byte_ors[3] = |(operand_a[31:24]);
        byte_ors[2] = |(operand_a[23:16]);
        byte_ors[1] = |(operand_a[15: 8]);
        byte_ors[0] = |(operand_a[ 7: 0]);
    end

    wire [31:0] orc_b = {{8{byte_ors[3]}}, {8{byte_ors[2]}}, {8{byte_ors[1]}}, {8{byte_ors[0]}}};

    logic [31:0] unary_result_mux;
    always_comb begin
        case (micro_op.unary_result)
            UNARY_BREV   : unary_result_mux = byte_swap;
            UNARY_LZCNT  : unary_result_mux = 32'(zero_count);
            UNARY_ORC    : unary_result_mux = orc_b;
            UNARY_POPCNT : unary_result_mux = 32'(pop_count);
            UNARY_SEXT_B : unary_result_mux = unary_sext_b;
            UNARY_SEXT_H : unary_result_mux = unary_sext_h;
            default      : unary_result_mux = 'x;
        endcase
    end

    // -----------------------------------------------------------------------------------------------
    // SHIFTER
    // Used by most bit manipulation instructions and the simple shifting instructions

    // Shifter input multiplexer
    logic [31:0] shifter_in;
    always_comb begin
        case (micro_op.shifter_input)
            SHIFT_OP_A : shifter_in = operand_a;
            SHIFT_IMM  : shifter_in = extended_immediate;
        endcase
    end

    // Shifter - feeds the ALU
    logic [31:0] shifted;
    wire [4:0] shift_amount = micro_op.bypass_b_shift ? bypass_reg[4:0] : micro_op.shift;
    R5L_Shifter #(.WIDTH (32)) shifter (
        .in               (shifter_in),
        .in_valid         ('1),
        .in_ready         (),
        .out              (shifted),
        .out_valid        (),
        .out_ready        ('1),
        .shift            (shift_amount),
        .shift_left       (micro_op.shift_left),
        .rotate           (micro_op.rotate),
        .arithmetic_shift (micro_op.arithmetic_shift),
        .clk
    );

    // -----------------------------------------------------------------------------------------------
    // OPERAND B INVERTER FOR ADDITION AND LOGIC
    // Inverter is used for subtraction and negated logical operators (eg ANDN)
    logic [31:0] inverted_b;
    always_comb begin
        if (micro_op.invert_b) inverted_b = ~operand_b;
        else                   inverted_b = operand_b;
    end

    // -----------------------------------------------------------------------------------------------
    // ADDER AND BRANCH CONTROL
    // Handles addition, subtraction, comparison, and OR reduction
    wire [31:0] adder_input_b = (micro_op.store | micro_op.load) ? extended_immediate : inverted_b;
    logic [32:0] adder_result;
    always_comb begin
        adder_result = shifted + adder_input_b + 32'(micro_op.invert_b);
    end

    // Comparisons - signed and unsigned indications of A < B and A == B
    // For unsigned, check the carry out from the adder, and for signed, check whether overflow != sign bit
    wire signed_overflow = (~inverted_b[31] & ~shifted[31] &  adder_result[31]) |
                           ( inverted_b[31] &  shifted[31] & ~adder_result[31]);
    wire signed_less = adder_result[31] != signed_overflow;
    wire unsigned_less = ~adder_result[32];
    wire zero = adder_result[31:0] == '0;

    // Determine the branch direction
    logic branch_decision;
    always_comb begin
        case (micro_op.branch_type)
            NO_BRANCH     : branch_decision = '0;
            DATA_BRANCH   : branch_decision = '1;
            SIGNED_LESS   : branch_decision = signed_less;
            SIGNED_GE     : branch_decision = ~signed_less;
            UNSIGNED_LESS : branch_decision = unsigned_less;
            UNSIGNED_GE   : branch_decision = ~unsigned_less;
            EQUAL_BRANCH  : branch_decision = zero;
            NEQ_BRANCH    : branch_decision = ~zero;
        endcase
    end
    always_comb begin
        mispredict_valid = uop_valid & micro_op.branch_instruction &
                           (branch_decision ^ micro_op.branch_predicted);
        mispredict_pc    = micro_op.branch_type == DATA_BRANCH ? adder_result[31:0] :
                                                                 micro_op.alt_branch_target;
    end

    // For min and max, add a mux
    wire [31:0] cmp_mux = branch_decision ? operand_a : operand_b;

    // -----------------------------------------------------------------------------------------------
    // LOGIC UNIT
    // Used for logical operations and bit manipulation
    logic [31:0] logic_input_b;
    always_comb begin
        case (micro_op.shifter_input)
            SHIFT_IMM : logic_input_b = operand_a;
            default   : logic_input_b = inverted_b;
        endcase
    end

    logic [31:0] logic_result;
    always_comb begin
        case (micro_op.logical_operation)
            LOGICAL_AND    : logic_result = shifted & logic_input_b;
            LOGICAL_OR     : logic_result = shifted | logic_input_b;
            LOGICAL_XOR    : logic_result = shifted ^ logic_input_b;
            LOGICAL_PASS_A : logic_result = shifted;
            default        : logic_result = 'x;
        endcase
    end

    // -----------------------------------------------------------------------------------------------
    // LOAD/STORE UNIT
    // Issue loads and stores, but loads retire elsewhere
    always_comb begin
        data_write_en = uop_valid & backend_ack & micro_op.store;
        data_read_en  = uop_valid & backend_ack & micro_op.load;
        data_wdata    = operand_b;
        data_addr     = adder_result[31:0];
        case (micro_op.mem_width)
            MEMORY_BYTE : data_wmask = 4'b0001;
            MEMORY_HALF : data_wmask = 4'b0011;
            MEMORY_WORD : data_wmask = 4'b1111;
            default     : data_wmask = 'x;
        endcase
    end

    // -----------------------------------------------------------------------------------------------
    // MULTIPLIER
    // TODO

    // -----------------------------------------------------------------------------------------------
    // RESULTS AND WRITEBACK
    logic [31:0] result;
    always_comb begin
        case (micro_op.result_source)
            ADDER_RESULT      : result = adder_result[31:0];
            LOGIC_RESULT      : result = logic_result;
            COMPARISON_RESULT : result = cmp_mux;
            UNARY_RESULT      : result = unary_result_mux;
            BRANCH_BIT        : result = {31'd0, branch_decision};
            BRANCH_TARGET     : result = micro_op.alt_branch_target;
            default           : result = 'x;
        endcase
    end
    always_ff @(posedge clk) begin
        if (uop_valid) bypass_reg <= result;
    end

    // Writeback control
    always_comb begin
        writeback_data    = result;
        writeback_addr    = micro_op.destination;
        writeback_en      = uop_valid & (micro_op.result_source != NO_RESULT);
        writeback_is_read = micro_op.load;
        writeback_width   = micro_op.mem_width;
        writeback_signed  = micro_op.arithmetic_shift;
    end

endmodule
