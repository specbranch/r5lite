// Simple modules for a full adder and a 4:2 compressor.  Not necessarily optimal for FPGA use.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_FullAdder (
    input  wire  [2:0] in,
    output logic [1:0] out
);
    always_comb begin
        case (in[2:0])
            3'd0             : out = 2'd0;
            3'd1, 3'd2, 3'd4 : out = 2'd1;
            3'd3, 3'd5, 3'd6 : out = 2'd2;
            3'd7             : out = 2'd3;
        endcase
    end
endmodule

module R5L_FourCompressor (
    input  wire        cin,
    input  wire  [3:0] bits,
    output logic [1:0] out,
    output logic       cout
);
    // A 4:2 compressor is 2 full adders chained
    logic [1:0] first_adder;
    R5L_FullAdder inputAdder (
        .in  (bits[2:0]),
        .out (first_adder)
    );
    always_comb cout = first_adder[1];
    R5L_FullAdder outputAdder (
        .in  ({bits[3], first_adder[0], cin}),
        .out (out)
    );
endmodule
