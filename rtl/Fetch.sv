// Fetch unit for the R5Lite microprocessor core, accommodating variable latency loads
// 
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_Fetch (
    output logic [31:0] inst_addr,
    output logic        inst_read_en,
    input  wire         inst_ready,
    input  wire  [31:0] inst_rdata,

    output logic [31:0] decode_inst,
    output logic [31:0] decode_pc,
    output logic        decode_valid,
    input  wire         decode_ack,

    // Branch predictor link
    input  wire  [31:0] predict_pc,
    input  wire         predict_valid,

    // Branch signal from the execute stage, in case we were wrong
    input  wire  [31:0] mispredict_pc,
    input  wire         mispredict_valid,

    input  wire         run,
    input  wire         reset,
    input  wire         clk
);

    wire advance_pc = run & decode_ack;

    logic [31:0] pc, last_pc;
    always_ff @(posedge clk) begin
        if (reset) last_pc <= -4;
        else if (advance_pc) begin
            last_pc <= pc;
        end
    end
    always_comb begin
        if (mispredict_valid)   pc = mispredict_pc;
        else if (predict_valid) pc = predict_pc;
        else                    pc = last_pc + 32'd4;
    end

    // Instruction memory address
    always_comb begin
        inst_addr    = pc;
        inst_read_en = advance_pc;
    end

    // Pass instruction details to the decoder using small FIFOs, which need to cover
    // instruction read latency and read latency variance
    //
    // TODO: parametrize PTR_WIDTH on memory latency for configurable systems
    // TODO: possibly use the full signal
    R5L_FlushableFifo #(.DATA_WIDTH(32), .ADDR_WIDTH(32), .PTR_WIDTH(3)) instFifo (
        .data       (inst_rdata),
        .write_data (inst_ready),
        .addr       (inst_addr),
        .write_addr (inst_read_en),

        .full       (),
        .flush      (mispredict_valid),

        .read_data  (decode_inst),
        .read_addr  (decode_pc),
        .read_ack   (decode_ack),
        .read_valid (decode_valid),

        .reset,
        .clk
    );

endmodule
