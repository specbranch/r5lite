// Internal structs used by R5Lite
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

package R5L_Internal_pkg;

typedef enum logic [1:0] {
    LOGICAL_AND,
    LOGICAL_OR,
    LOGICAL_XOR,
    LOGICAL_PASS_A
} logic_op_t;

typedef enum logic [0:0] {
    SHIFT_IMM,
    SHIFT_OP_A
} shifter_select_t;

typedef enum logic [2:0] {
    NO_BRANCH,
    DATA_BRANCH,
    SIGNED_LESS,
    SIGNED_GE,
    UNSIGNED_LESS,
    UNSIGNED_GE,
    EQUAL_BRANCH,
    NEQ_BRANCH
} branch_type_t;

typedef enum logic [2:0] {
    UNARY_SEXT_B,
    UNARY_SEXT_H,
    UNARY_POPCNT,
    UNARY_BREV,
    UNARY_LZCNT,
    UNARY_ORC
} unary_result_t;

typedef enum logic [2:0] {
    NO_RESULT,
    ADDER_RESULT,
    LOGIC_RESULT,
    COMPARISON_RESULT,
    UNARY_RESULT,
    BRANCH_BIT,         // used for SLT/SLTU
    BRANCH_TARGET       // used for JAL/JALR
} result_source_t;

typedef enum logic [1:0] {
    MEMORY_BYTE,
    MEMORY_HALF,
    MEMORY_WORD
} memory_width_t;

typedef struct packed {
    // Branching information - whether the branch was predicted and where to go if we were wrong
    logic [31:0]     alt_branch_target;
    logic            branch_predicted;
    logic            branch_instruction;

    // Operand selection
    logic [31:0]     operand_a;
    logic            bypass_a;
    logic [31:0]     operand_b;
    logic            bypass_b;
    logic            bypass_b_shift;

    // ALU command structure
    logic            invert_b;          // Used to invert B or reverse A for TZCNT
    logic [4:0]      shift;
    logic_op_t       logical_operation;
    shifter_select_t shifter_input;
    branch_type_t    branch_type;
    logic            shift_left;
    logic            rotate;
    logic            arithmetic_shift;
    unary_result_t   unary_result;

    // Load/store command
    logic            load;
    logic            store;
    logic [11:0]     short_immediate;

    // Result writeback control
    result_source_t  result_source;
    logic [4:0]      destination;

    // For load/store width
    memory_width_t   mem_width;
} r5l_micro_op_t;

endpackage
