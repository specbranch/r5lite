// Generic shifter module in SystemVerilog
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_Shifter #(
    parameter int WIDTH,
    parameter int PIPE_STAGES = 0,     // pipe stages will be evenly distributed through the shifter
    parameter int SHIFT_WIDTH = $clog2(WIDTH)
) (
    input  wire  [WIDTH - 1:0]         in,
    input  wire                        in_valid,
    output logic                       in_ready,

    output logic [WIDTH - 1:0]         out,
    output logic                       out_valid,
    input  wire                        out_ready,

    input  wire  [SHIFT_WIDTH - 1 : 0] shift,
    input  wire                        shift_left,
    input  wire                        rotate,
    input  wire                        arithmetic_shift,

    input  wire  clk
);

    // Carry the shift and intermediate signals through the shifter
    logic [2 * WIDTH - 2 : 0] stage [SHIFT_WIDTH + 1] /* verilator split_var */;
    logic [SHIFT_WIDTH - 1 : 0] shift_pipe [SHIFT_WIDTH + 1] /* verilator split_var */;
    logic valid_pipe [SHIFT_WIDTH + 1] /* verilator split_var */;
    logic ready_pipe [SHIFT_WIDTH + 1] /* verilator split_var */;

    // Set up initial conditions for the funnel shifter
    always_comb begin
        if (!shift_left) begin
            stage[0] = {(rotate ? in[WIDTH - 2 : 0] :
                         arithmetic_shift ? {(WIDTH - 1){in[WIDTH - 1]}} :
                         (WIDTH - 1)'(0)), in};
        end
        else begin
            stage[0] = {in, rotate ? in[WIDTH - 1 : 1] : (WIDTH - 1)'(0)};
        end

        shift_pipe[0] = shift_left ? (SHIFT_WIDTH'(WIDTH - 1) - shift) : shift;
        valid_pipe[0] = in_valid;
    end
    always_comb in_ready = ready_pipe[0];

    genvar n;
    generate
        for (n = 0; n < SHIFT_WIDTH; n++) begin : gen_shift_stage
            localparam int shift_amount = 2 ** n;
        
            logic [2 * WIDTH - 2 : 0] intermediate;
            always_comb intermediate = shift_pipe[n][n] ? (stage[n] >> shift_amount) : stage[n];
        
            // Each shifter stage, add PIPE_STAGES "pipeline points" and when you get over a boundary
            // of size SHIFT_WIDTH, then add a pipe stage, and if not, don't add a pipe stage yet
            if (PIPE_STAGES * (n + 1) / SHIFT_WIDTH != PIPE_STAGES * n / SHIFT_WIDTH) begin : gen_pipe
                always_ff @(posedge clk) begin
                    if (ready_pipe[n]) begin
                        stage[n + 1]      <= intermediate;
                        shift_pipe[n + 1] <= shift_pipe[n];
                        valid_pipe[n + 1] <= valid_pipe[n];
                    end
                end
                always_comb ready_pipe[n] = ready_pipe[n + 1] | ~valid_pipe[n + 1];
            end
            else begin: gen_unpipelined
                /* verilator lint_off ALWCOMBORDER */
                always_comb begin
                    stage[n + 1]      = intermediate;
                    shift_pipe[n + 1] = shift_pipe[n];
                    valid_pipe[n + 1] = valid_pipe[n];
                end
                always_comb ready_pipe[n] = ready_pipe[n + 1];
                /* verilator lint_on ALWCOMBORDER */
            end
        end
    endgenerate

    always_comb begin
        out       = stage[SHIFT_WIDTH][WIDTH - 1 : 0];
        out_valid = valid_pipe[SHIFT_WIDTH];
    end
    always_comb ready_pipe[SHIFT_WIDTH] = out_ready;

endmodule