// Module for a byte-oriented RAM, planned to be used as a TCM in R5Lite systems
//
// Uses a small set of shifters and a set of byte-wide memories to achieve byte-orientation and
// perform unaligned loads and stores with no penalty.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_ByteRam #(
    parameter int ADDR_WIDTH,
    parameter int DATA_BYTES
) (
    input  wire                            read_en,
    input  wire                            write_en,
    input  wire  [ADDR_WIDTH - 1 : 0]      address,
    input  wire  [DATA_BYTES - 1 : 0][7:0] write_data,
    input  wire  [DATA_BYTES - 1 : 0]      write_mask,

    output logic                           read_ready,
    output logic [DATA_BYTES - 1 : 0][7:0] read_data,

    input  wire                            clk,
    input  wire                            reset
);

    logic [ADDR_WIDTH - $clog2(DATA_BYTES) - 1 : 0] word_select;
    logic [$clog2(DATA_BYTES) - 1 : 0]              byte_offset;
    always_comb begin
        word_select = address[ADDR_WIDTH - 1 : $clog2(DATA_BYTES)];
        byte_offset = address[$clog2(DATA_BYTES) - 1 : 0];
    end

    localparam int WORDS = 2 ** ADDR_WIDTH / DATA_BYTES;

    logic [DATA_BYTES - 1 : 0][7:0] lane_write, lane_read;
    logic [DATA_BYTES - 1 : 0]      lane_mask;
    generate
        genvar n;
        for (n = 0; n < DATA_BYTES; n += 1) begin : gen_lane
            logic [7:0] ram [WORDS];

            logic [ADDR_WIDTH - $clog2(DATA_BYTES) - 1 : 0] lane_addr;
            /* verilator lint_off CMPCONST */
            always_comb lane_addr = word_select + ((byte_offset > n) ? 1 : 0);
            /* verilator lint_on CMPCONST */

            always_ff @(posedge clk) begin
                if (write_en && lane_mask[n]) ram[lane_addr] <= lane_write[n];
                if (read_en)                  lane_read[n]   <= ram[lane_addr];
            end
        end
    endgenerate

    // Hook up lane read and lane write: rotate left to write and right to read
    R5L_Shifter #(.WIDTH(DATA_BYTES * 8)) writeShift (
        .in               (write_data),
        .in_valid         ('1),
        .in_ready         (),
        .out              (lane_write),
        .out_valid        (),
        .out_ready        ('1),
        .shift            ({byte_offset, 3'd0}),
        .shift_left       ('1),
        .rotate           ('1),
        .arithmetic_shift ('0),
        .clk
    );
    R5L_Shifter #(.WIDTH(DATA_BYTES)) maskShift (
        .in               (write_mask),
        .in_valid         ('1),
        .in_ready         (),
        .out              (lane_mask),
        .out_valid        (),
        .out_ready        ('1),
        .shift            (byte_offset),
        .shift_left       ('1),
        .rotate           ('1),
        .arithmetic_shift ('0),
        .clk
    );
    
    logic [$clog2(DATA_BYTES) - 1 : 0] last_byte_offset;
    always_ff @(posedge clk) begin
        last_byte_offset <= byte_offset;
    end
    R5L_Shifter #(.WIDTH(DATA_BYTES * 8)) readShift (
        .in               (lane_read),
        .in_valid         ('1),
        .in_ready         (),
        .out              (read_data),
        .out_valid        (),
        .out_ready        ('1),
        .shift            ({last_byte_offset, 3'd0}),
        .shift_left       ('0),
        .rotate           ('1),
        .arithmetic_shift ('0),
        .clk
    );

    // Read ready signal
    always_ff @(posedge clk) begin
        if (reset) read_ready <= '0;
        else       read_ready <= read_en;
    end

endmodule
