// Decoder unit for the R5Lite Microprocessor
//
// Turns instructions into commands for the backend, predicts the next PC, and reads from the
// register file.  Also tracks things like pipeline stalls based on loads, bypass register
// usage, and trapping on an ECALL or EBREAK instruction.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.
//
// TODO: Add compressed instructions
// TODO: Possibly add trapping on invalid instructions

module R5L_Decode #(
    parameter bit M_INSTRUCTIONS = 1'b1
)(
    input  wire  [31:0]                     decode_inst,
    input  wire  [31:0]                     decode_pc,
    input  wire                             decode_valid,
    output logic                            decode_ack,

    output R5L_Internal_pkg::r5l_micro_op_t micro_op,
    output logic                            uop_valid,
    input  wire                             backend_ack,

    output logic [4:0]                      read_a_addr,
    output logic [4:0]                      read_b_addr,
    input  wire  [31:0]                     read_a_data,
    input  wire  [31:0]                     read_b_data,

    output logic [31:0]                     predict_pc,
    output logic                            predict_valid,

    input  wire                             mispredicted,
    input  wire                             load_ended,
    input  wire                             store_ended,
    output logic                            trap,
    input  wire                             run,

    input  wire                             reset,
    input  wire                             clk
);

    import R5L_Internal_pkg::*;
    import RVInst_pkg::*;

    // Instruction parsing
    instruction_t instruction;
    always_comb instruction = decode_inst;

    decode_type_t inst_type;
    always_comb inst_type = decode_type(instruction);

    wire [4:0] inst_dest  = get_dest(instruction);
    wire [4:0] inst_src_a = get_src_a(instruction);
    wire [4:0] inst_src_b = get_src_b(instruction);
    wire [6:0] inst_func7 = get_func7(instruction);

    // Bypassing control
    logic [4:0] last_dest;
    always_ff @(posedge clk) begin
        if (decode_valid && decode_ack) last_dest <= inst_dest;
    end

    logic bypass_ready;
    always_ff @(posedge clk) begin
        if (reset) bypass_ready <= '0;
        else if (decode_valid && decode_ack) begin
            case (opcode_t'(instruction.opcode))
                OPCODE_OP,
                OPCODE_OP_IMM,
                OPCODE_JAL,
                OPCODE_JALR,
                OPCODE_LUI,
                OPCODE_AUIPC : bypass_ready <= !mispredicted && (inst_dest != '0);
                default      : bypass_ready <= '0;
            endcase
        end
    end

    wire bypass_match_a = bypass_ready && (last_dest == inst_src_a);
    wire bypass_match_b = bypass_ready && (last_dest == inst_src_b);

    // Register read
    always_comb begin
        read_a_addr = inst_src_a;
        read_b_addr = inst_src_b;
    end

    // Immediate retrieval
    wire [31:0] immediate = get_immediate(instruction, inst_type);
    wire [31:0] i_imm = i_immediate(instruction_i_t'(instruction.tail));

    // Branch prediction
    wire [31:0] jump_pc = immediate + decode_pc;
    wire backward_branch = (instruction.opcode == OPCODE_BRANCH) && immediate[31];
    always_comb begin
        predict_valid = decode_valid && ((instruction.opcode == OPCODE_JAL) || backward_branch);
        predict_pc    = jump_pc;
    end

    // Tracker for memory instructions - only allow one outstanding load or store
    wire mem_instruction = (instruction.opcode == OPCODE_LOAD) || (instruction.opcode == OPCODE_STORE);
    wire mem_done = load_ended | store_ended;

    logic in_mem_access;
    always_ff @(posedge clk) begin
        if (reset) in_mem_access <= '0;
        else begin
            if (mem_instruction && decode_ack && decode_valid) begin
                in_mem_access <= '1;
            end
            else if (mem_done) in_mem_access <= '0;
        end
    end
    wire mem_conflict = in_mem_access && mem_instruction;

    // Tracker for load targets - stall if we are loading from a register whose value hasn't been loaded
    logic in_load;
    logic [4:0] load_target;
    always_ff @(posedge clk) begin
        if (reset) in_load <= '0;
        else begin
            if (instruction.opcode == OPCODE_LOAD && decode_ack && decode_valid) begin
                in_load <= instruction.opcode == OPCODE_LOAD;
                load_target   <= inst_dest;
            end
            else if (load_ended) in_load <= '0;
        end
    end

    // Check loads and stores to operand registers, and delay
    wire load_target_match_d  = (inst_dest == load_target) && (inst_type != B_TYPE) && (inst_type != S_TYPE);
    wire load_target_match_s1 = (inst_src_a == load_target) && (inst_type != U_TYPE) && (inst_type != J_TYPE);
    wire load_target_match_s2 = (inst_src_b == load_target) && (inst_type != U_TYPE) && (inst_type != J_TYPE) &&
                                (inst_type != I_TYPE);
    wire load_target_match = load_target_match_d | load_target_match_s1 | load_target_match_s2;
    wire load_conflict = in_load && load_target != 0 && load_target_match;

    // ACK if we are done with the instruction
    // TODO: Add conditions for the DIV state machine
    always_comb begin
        if (in_mem_access) begin
            // One memory instruction inflight at a time, if we have an instruction, check for a load conflict
            decode_ack = ~load_conflict & ~mem_conflict & backend_ack;
        end
        else begin
            decode_ack = backend_ack;
        end
    end

    // Valid SYSTEM instructions are ECALL and EBREAK, which we treat as HALT
    always_comb begin
        trap = decode_ack && decode_valid && !mispredicted && instruction.opcode == OPCODE_SYSTEM;
    end

    // Micro-op decode
    wire [2:0] inst_i_func3_bits = get_i_func3_bits(instruction);
    R5L_Internal_pkg::r5l_micro_op_t decoded_uop;
    always_comb begin
        decoded_uop = 'x;

        // Defaults for fields that have an effect on everything
        decoded_uop.load               = instruction.opcode == OPCODE_LOAD;
        decoded_uop.store              = instruction.opcode == OPCODE_STORE;
        decoded_uop.short_immediate    = immediate[11:0];
        decoded_uop.branch_predicted   = predict_valid;
        decoded_uop.branch_instruction = '0;
        decoded_uop.destination        = inst_dest;
        decoded_uop.bypass_a           = '0;
        decoded_uop.bypass_b           = '0;
        decoded_uop.shifter_input      = SHIFT_OP_A;
        decoded_uop.shift              = '0;
        decoded_uop.bypass_b_shift     = '0;
        decoded_uop.invert_b           = '0;

        case (opcode_t'(instruction.opcode))
            OPCODE_LOAD : begin
                decoded_uop.operand_a     = read_a_data;
                decoded_uop.operand_b     = read_b_data;
                decoded_uop.bypass_a      = bypass_match_a;
                decoded_uop.bypass_b      = bypass_match_b;
                // Placeholder that isn't NO_RESULT - will be discarded
                decoded_uop.result_source = ADDER_RESULT;
                case (inst_i_func3_bits[1:0])
                    2'd0    : decoded_uop.mem_width = MEMORY_BYTE;
                    2'd1    : decoded_uop.mem_width = MEMORY_HALF;
                    2'd2    : decoded_uop.mem_width = MEMORY_WORD;
                    default : decoded_uop.mem_width = memory_width_t'('x);
                endcase
                decoded_uop.arithmetic_shift = ~inst_i_func3_bits[2];
            end
            OPCODE_STORE : begin
                decoded_uop.operand_a     = read_a_data;
                decoded_uop.operand_b     = read_b_data;
                decoded_uop.bypass_a      = bypass_match_a;
                decoded_uop.bypass_b      = bypass_match_b;
                decoded_uop.result_source = NO_RESULT;
                case (get_s_func3_bits(instruction))
                    3'd0    : decoded_uop.mem_width = MEMORY_BYTE;
                    3'd1    : decoded_uop.mem_width = MEMORY_HALF;
                    3'd2    : decoded_uop.mem_width = MEMORY_WORD;
                    default : decoded_uop.mem_width = memory_width_t'('x);
                endcase
            end
            OPCODE_AUIPC : begin
                decoded_uop.operand_a     = decode_pc;
                decoded_uop.operand_b     = immediate;
                decoded_uop.result_source = ADDER_RESULT;
            end
            OPCODE_LUI : begin
                decoded_uop.operand_a       = 'x;
                decoded_uop.operand_b       = immediate;
                decoded_uop.short_immediate = '0;
                decoded_uop.result_source   = ADDER_RESULT;
                decoded_uop.shifter_input   = SHIFT_IMM;
            end
            OPCODE_JAL : begin
                decoded_uop.operand_a          = 'x;
                decoded_uop.operand_b          = 'x;
                decoded_uop.branch_instruction = '1;
                decoded_uop.alt_branch_target  = decode_pc + 32'd4;
                decoded_uop.result_source      = BRANCH_TARGET;
                decoded_uop.branch_type        = DATA_BRANCH;
            end
            OPCODE_JALR : begin
                decoded_uop.operand_a          = read_a_data;
                decoded_uop.operand_b          = immediate;
                decoded_uop.branch_instruction = '1;
                decoded_uop.alt_branch_target  = decode_pc + 32'd4;
                decoded_uop.result_source      = BRANCH_TARGET;
                decoded_uop.branch_type        = DATA_BRANCH;
                decoded_uop.bypass_a           = bypass_match_a;
            end
            OPCODE_BRANCH : begin
                decoded_uop.operand_a          = read_a_data;
                decoded_uop.operand_b          = read_b_data;
                decoded_uop.bypass_a           = bypass_match_a;
                decoded_uop.bypass_b           = bypass_match_b;
                decoded_uop.invert_b           = '1;
                decoded_uop.branch_instruction = '1;
                decoded_uop.alt_branch_target  = backward_branch ? (decode_pc + 32'd4) : jump_pc;
                decoded_uop.result_source      = NO_RESULT;

                case (get_b_func3(instruction))
                    FUNC_BEQ  : decoded_uop.branch_type = EQUAL_BRANCH;
                    FUNC_BNE  : decoded_uop.branch_type = NEQ_BRANCH;
                    FUNC_BLT  : decoded_uop.branch_type = SIGNED_LESS;
                    FUNC_BGE  : decoded_uop.branch_type = SIGNED_GE;
                    FUNC_BLTU : decoded_uop.branch_type = UNSIGNED_LESS;
                    FUNC_BGEU : decoded_uop.branch_type = UNSIGNED_GE;
                    default   : decoded_uop.branch_type = branch_type_t'('x);
                endcase
            end
            OPCODE_OP_IMM : begin
                decoded_uop.operand_a       = read_a_data;
                decoded_uop.bypass_a        = bypass_match_a;
                decoded_uop.operand_b       = immediate;
                decoded_uop.result_source   = LOGIC_RESULT;

                case (get_op_func3(instruction))
                    FUNC_ADD : begin
                        decoded_uop.result_source = ADDER_RESULT;
                    end
                    FUNC_SLT : begin
                        decoded_uop.result_source = BRANCH_BIT;
                        decoded_uop.branch_type   = SIGNED_LESS;
                        decoded_uop.invert_b      = '1;
                    end
                    FUNC_SLTU : begin
                        decoded_uop.result_source = BRANCH_BIT;
                        decoded_uop.branch_type   = UNSIGNED_LESS;
                        decoded_uop.invert_b      = '1;
                    end
                    FUNC_XOR : decoded_uop.logical_operation = LOGICAL_XOR;
                    FUNC_OR  : decoded_uop.logical_operation = LOGICAL_OR;
                    FUNC_AND : decoded_uop.logical_operation = LOGICAL_AND;
                    FUNC_SLL : begin
                        decoded_uop.shift = immediate[4:0];

                        // Bit manipulation instructions
                        if (i_imm[7]) begin
                            decoded_uop.shift_left = '1;

                            if (i_imm[10:9] == 2'b01) begin
                                // BSETI
                                decoded_uop.short_immediate   = 12'd1;
                                decoded_uop.shifter_input     = SHIFT_IMM;
                                decoded_uop.logical_operation = LOGICAL_OR;
                                decoded_uop.rotate            = 'x;
                            end
                            else if (i_imm[10:9] == 2'b10) begin
                                // BCLRI
                                decoded_uop.short_immediate   = ~(12'd1);
                                decoded_uop.shifter_input     = SHIFT_IMM;
                                decoded_uop.logical_operation = LOGICAL_AND;
                                decoded_uop.rotate            = '1;
                            end
                            else if (i_imm[10:9] == 2'b11) begin
                                // BINVI
                                decoded_uop.short_immediate   = 12'd1;
                                decoded_uop.shifter_input     = SHIFT_IMM;
                                decoded_uop.logical_operation = LOGICAL_XOR;
                                decoded_uop.rotate            = 'x;
                            end
                        end
                        else if (i_imm[10:9] == 2'b11) begin
                            decoded_uop.result_source = UNARY_RESULT;
                            if (i_imm[2:0] == 3'd0) begin
                                decoded_uop.invert_b     = '0;
                                decoded_uop.unary_result = UNARY_LZCNT;
                            end
                            else if (i_imm[2:0] == 3'd1) begin
                                decoded_uop.invert_b     = '1;
                                decoded_uop.unary_result = UNARY_LZCNT;
                            end
                            else if (i_imm[2:0] == 3'd2) begin
                                decoded_uop.unary_result = UNARY_POPCNT;
                            end
                            else if (i_imm[2:0] == 3'd4) begin
                                decoded_uop.unary_result = UNARY_SEXT_B;
                            end
                            else if (i_imm[2:0] == 3'd5) begin
                                decoded_uop.unary_result = UNARY_SEXT_H;
                            end
                        end
                        else begin
                            decoded_uop.logical_operation = LOGICAL_PASS_A;
                            decoded_uop.rotate            = '0;
                            decoded_uop.shift_left        = '1;
                        end
                    end
                    FUNC_SRLA : begin
                        decoded_uop.shift = immediate[4:0];

                        if (i_imm[7]) begin
                            // Alternate bit manipulation instructions
                            if (i_imm[10:9] == 2'b01) begin
                                // ORC.B
                                decoded_uop.result_source = UNARY_RESULT;
                                decoded_uop.unary_result  = UNARY_ORC;
                            end
                            else if (i_imm[10:9] == 2'b10) begin
                                // BEXTI - shift and then mask
                                decoded_uop.shift_left        = '0;
                                decoded_uop.logical_operation = LOGICAL_AND;
                                decoded_uop.operand_b         = 32'd1;
                            end
                            else if (i_imm[10:9] == 2'b11) begin
                                // REV8
                                decoded_uop.result_source = UNARY_RESULT;
                                decoded_uop.unary_result  = UNARY_BREV;
                            end
                        end
                        else begin
                            decoded_uop.logical_operation = LOGICAL_PASS_A;
                            decoded_uop.arithmetic_shift  = i_imm[10];
                            decoded_uop.rotate            = i_imm[9];
                            decoded_uop.shift_left        = '0;
                        end
                    end
                endcase
            end
            OPCODE_OP : begin
                decoded_uop.operand_a     = read_a_data;
                decoded_uop.operand_b     = read_b_data;
                decoded_uop.bypass_a      = bypass_match_a;
                decoded_uop.bypass_b      = bypass_match_b;
                decoded_uop.result_source = LOGIC_RESULT;

                if (M_INSTRUCTIONS && inst_func7 == 7'd1) begin
                    // TODO: MUL/DIV instructions
                end
                else if (inst_func7 == 7'd5) begin
                    decoded_uop.result_source = COMPARISON_RESULT;
                    decoded_uop.invert_b      = '1;

                    case (get_minmax_clmul_func3(instruction))
                        FUNC_MIN  : decoded_uop.branch_type = SIGNED_LESS;
                        FUNC_MINU : decoded_uop.branch_type = UNSIGNED_LESS;
                        FUNC_MAX  : decoded_uop.branch_type = SIGNED_GE;
                        FUNC_MAXU : decoded_uop.branch_type = UNSIGNED_GE;
                        default   : decoded_uop.branch_type = branch_type_t'('x);
                    endcase
                end
                else begin
                    case (get_op_func3(instruction))
                        FUNC_ADD : begin
                            decoded_uop.result_source = ADDER_RESULT;
                            decoded_uop.invert_b      = inst_func7[5];
                        end
                        FUNC_SLT : begin
                            if (inst_func7[4]) begin
                                decoded_uop.result_source = ADDER_RESULT;
                                decoded_uop.shift_left    = '1;
                                decoded_uop.rotate        = '0;
                                decoded_uop.shift         = 5'd1;
                            end
                            else begin
                                decoded_uop.result_source = BRANCH_BIT;
                                decoded_uop.branch_type   = SIGNED_LESS;
                                decoded_uop.invert_b      = '1;
                            end
                        end
                        FUNC_SLTU : begin
                            decoded_uop.result_source = BRANCH_BIT;
                            decoded_uop.branch_type   = UNSIGNED_LESS;
                            decoded_uop.invert_b      = '1;
                        end
                        FUNC_XOR : begin
                            if (inst_func7[4]) begin
                                // SH2ADD
                                decoded_uop.result_source = ADDER_RESULT;
                                decoded_uop.shift_left    = '1;
                                decoded_uop.rotate        = '0;
                                decoded_uop.shift         = 5'd2;
                            end
                            else if (inst_func7[2] && (inst_src_b == 0)) begin
                                // ZEXT.H - weirdly inside the "OP" opcodes, not "OP-IMM"
                                decoded_uop.bypass_b          = '0;
                                decoded_uop.operand_b         = 32'h0000ffff;
                                decoded_uop.logical_operation = LOGICAL_AND;
                            end
                            else begin
                                decoded_uop.logical_operation = LOGICAL_XOR;
                                decoded_uop.invert_b          = inst_func7[5];
                            end
                        end
                        FUNC_OR  : begin
                            if (inst_func7[4]) begin
                                // SH3ADD
                                decoded_uop.result_source = ADDER_RESULT;
                                decoded_uop.shift_left    = '1;
                                decoded_uop.rotate        = '0;
                                decoded_uop.shift         = 5'd3;
                            end
                            else begin
                                decoded_uop.logical_operation = LOGICAL_OR;
                                decoded_uop.invert_b          = inst_func7[5];
                            end
                        end
                        FUNC_AND : begin
                            decoded_uop.logical_operation = LOGICAL_AND;
                            decoded_uop.invert_b          = inst_func7[5];
                        end
                        FUNC_SLL : begin
                            decoded_uop.shift          = read_b_data[4:0];
                            decoded_uop.shift_left     = '1;
                            decoded_uop.bypass_b_shift = bypass_match_b;
                            decoded_uop.bypass_b       = 'x;

                            // Bit manipulation instructions
                            if (inst_func7[2]) begin
                                if (inst_func7[5:4] == 2'b01) begin
                                    // BSET
                                    decoded_uop.short_immediate   = 12'd1;
                                    decoded_uop.shifter_input     = SHIFT_IMM;
                                    decoded_uop.logical_operation = LOGICAL_OR;
                                    decoded_uop.rotate            = 'x;
                                end
                                else if (inst_func7[5:4] == 2'b10) begin
                                    // BCLR
                                    decoded_uop.short_immediate   = ~(12'd1);
                                    decoded_uop.shifter_input     = SHIFT_IMM;
                                    decoded_uop.logical_operation = LOGICAL_AND;
                                    decoded_uop.rotate            = '1;
                                end
                                else if (inst_func7[5:4] == 2'b11) begin
                                    // BINV
                                    decoded_uop.short_immediate   = 12'd1;
                                    decoded_uop.shifter_input     = SHIFT_IMM;
                                    decoded_uop.logical_operation = LOGICAL_XOR;
                                    decoded_uop.rotate            = 'x;
                                end
                            end
                            else begin
                                decoded_uop.logical_operation = LOGICAL_PASS_A;
                                decoded_uop.rotate            = inst_func7[4];
                            end
                        end
                        FUNC_SRLA : begin
                            decoded_uop.shift          = read_b_data[4:0];
                            decoded_uop.shift_left     = '0;
                            decoded_uop.bypass_b       = 'x;
                            decoded_uop.bypass_b_shift = bypass_match_b;
                            if (inst_func7[2]) begin
                                // BEXT - shift and then mask
                                if (inst_func7[5:4] == 2'b10) begin
                                    decoded_uop.logical_operation = LOGICAL_AND;
                                    decoded_uop.operand_b         = 32'd1;
                                    decoded_uop.bypass_b          = '0;
                                end
                            end
                            else begin
                                decoded_uop.logical_operation = LOGICAL_PASS_A;
                                decoded_uop.arithmetic_shift  = inst_func7[5];
                                decoded_uop.rotate            = inst_func7[4];
                            end
                        end
                    endcase
                end
            end
            OPCODE_SYSTEM : begin
                decoded_uop.result_source = NO_RESULT;
            end
            OPCODE_MISC_MEM : begin
                // FENCE is a NOP for us
                decoded_uop.result_source = NO_RESULT;
            end
            default : begin
                // TODO: trap on bad opcodes
            end
        endcase
    end

    always_ff @(posedge clk) begin
        if (reset) uop_valid <= '0;
        else       uop_valid <= run & decode_valid & decode_ack & ~mispredicted;
    end
    always_ff @(posedge clk) begin
        if (decode_valid & decode_ack) begin
            micro_op <= decoded_uop;
        end
    end

endmodule
