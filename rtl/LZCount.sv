// Module for an FPGA- and ASIC-happy LZCount system, using a 4-input initial gate and then a small
// carry chain to combine results.
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

module R5L_LZCount #(
    parameter int WIDTH = 32,
    parameter int OUTPUT_WIDTH = $clog2(WIDTH) + 1
) (
    input  wire  [WIDTH - 1 : 0]        in,
    output logic [OUTPUT_WIDTH - 1 : 0] out
);
    // Generate nybble-sized counts and zero flags
    logic [1:0] nybble_count [WIDTH / 4];
    logic       nybble_zero  [WIDTH / 4];
    generate
        genvar n;
        for (n = 0; n < WIDTH / 4; n += 1) begin : gen_nybble
            always_comb begin
                casez (in[n * 4 + 3 : n * 4])
                    4'b1??? : nybble_count[n] = 2'd0;
                    4'b01?? : nybble_count[n] = 2'd1;
                    4'b001? : nybble_count[n] = 2'd2;
                    4'b0001 : nybble_count[n] = 2'd3;
                    4'b0000 : nybble_count[n] = 2'dx;
                endcase
                nybble_zero[n] = in[n * 4 + 3 : n * 4] == '0;
            end
        end
    endgenerate

    // "Scan" the bits to find the index we need - This should reduce to either
    // logic or a carry chain, not a chain of multiplexers
    logic found_one;
    logic [OUTPUT_WIDTH - 3 : 0] index;
    always_comb begin
        found_one = '0;
        index = 0;

        for (int i = WIDTH / 4 - 1; i >= 0; i--) begin
            if (!found_one && !nybble_zero[i]) begin
                index = (OUTPUT_WIDTH - 2)'(i);
                found_one = 1;
            end
        end
    end

    always_comb begin
        if (found_one) begin
            out[OUTPUT_WIDTH - 1 : 2] = (OUTPUT_WIDTH - 2)'(WIDTH / 4 - 1) - index;
            /* verilator lint_off WIDTH */
            out[1:0] = nybble_count[index];
            /* verilator lint_on WIDTH */
        end
        else begin
            out = OUTPUT_WIDTH'(WIDTH);
        end
    end

endmodule
