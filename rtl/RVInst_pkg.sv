// Helper constructs for parsing RISC-V instructions
//
// Copyright 2023 by Nima Badizadegan
//
// This source describes open hardware and is licensed under the CERN-OHL-W v2
//
// You may redistribute and modify this source code and make products using it under the terms of
// the CERN-OHL-W v2 (https://cern.ch/cern-ohl).  This source code is distributed WITHOUT ANY
// EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY  QUALITY AND FITNESS FOR
// A PARTICULAR PURPOSE.  Please see the CERN-OHL-W v2 for applicable conditions.
//
// Source location: https://gitlab.com/specbranch/r5lite
//
// As per CERN-OHL-W v2 section 4.1, should You produce hardware based on  these sources, You must
// maintain the Source Location visible on the external case of the product you make using this
// source code.

package RVInst_pkg;

    // Instruction field parsing structs
    // See the "RV32I Base Integer Instruction Set" section in the RISC-V spec
    typedef struct packed {
        logic [6:0] func7;
        logic [4:0] rs2;
        logic [4:0] rs1;
        logic [2:0] func3;
        logic [4:0] rd;
    } instruction_r_t;

    typedef struct packed {
        logic [11:0] imm;
        logic [4:0] rs1;
        logic [2:0] func3;
        logic [4:0] rd;
    } instruction_i_t;

    typedef struct packed {
        logic [6:0] imm_a;
        logic [4:0] rs2;
        logic [4:0] rs1;
        logic [2:0] func3;
        logic [4:0] imm_b;
    } instruction_s_t;

    typedef struct packed {
        logic [0:0] imm_a;
        logic [5:0] imm_c;
        logic [4:0] rs2;
        logic [4:0] rs1;
        logic [2:0] func3;
        logic [3:0] imm_d;
        logic [0:0] imm_b;
    } instruction_b_t;

    typedef struct packed {
        logic [19:0] imm;
        logic [4:0]  rd;
    } instruction_u_t;

    typedef struct packed {
        logic [0:0] imm_a;
        logic [9:0] imm_d;
        logic [0:0] imm_c;
        logic [7:0] imm_b;
        logic [4:0] rd;
    } instruction_j_t;

    // Functions for getting the immediate out of an instruction
    function automatic logic [31:0] i_immediate (instruction_i_t inst);
        return 32'(signed'(inst.imm));
    endfunction

    function automatic logic [31:0] s_immediate (instruction_s_t inst);
        return 32'(signed'({inst.imm_a, inst.imm_b}));
    endfunction

    function automatic logic [31:0] b_immediate (instruction_b_t inst);
        return 32'(signed'({inst.imm_a, inst.imm_b, inst.imm_c, inst.imm_d, 1'b0}));
    endfunction

    function automatic logic [31:0] u_immediate (instruction_u_t inst);
        return {inst.imm, 12'b0};
    endfunction

    function automatic logic [31:0] j_immediate (instruction_j_t inst);
        return 32'(signed'({inst.imm_a, inst.imm_b, inst.imm_c, inst.imm_d, 1'b0}));
    endfunction

    // Top-level instruction enum
    typedef enum logic [6:0] {
        OPCODE_LOAD      = 7'b0000011,
        OPCODE_LOAD_FP   = 7'b0000111,
        OPCODE_MISC_MEM  = 7'b0001111,
        OPCODE_OP_IMM    = 7'b0010011,
        OPCODE_AUIPC     = 7'b0010111,
        OPCODE_OP_IMM_32 = 7'b0011011,

        OPCODE_STORE     = 7'b0100011,
        OPCODE_STORE_FP  = 7'b0100111,
        OPCODE_AMO       = 7'b0101111,
        OPCODE_OP        = 7'b0110011,
        OPCODE_LUI       = 7'b0110111,
        OPCODE_OP_32     = 7'b0111011,

        OPCODE_MADD      = 7'b1000011,
        OPCODE_MSUB      = 7'b1000111,
        OPCODE_NMSUB     = 7'b1001011,
        OPCODE_NMADD     = 7'b1001111,
        OPCODE_OP_FP     = 7'b1010011,

        OPCODE_BRANCH    = 7'b1100011,
        OPCODE_JALR      = 7'b1100111,
        OPCODE_JAL       = 7'b1101111,
        OPCODE_SYSTEM    = 7'b1110011
    } opcode_t;

    // Instruction enums for specific instruction types
    typedef enum logic [2:0] {
        FUNC_JAL = 3'h0
    } jal_func3_t;
    typedef enum logic [2:0] {
        FUNC_BEQ  = 3'h0,
        FUNC_BNE  = 3'h1,
        FUNC_BLT  = 3'h4,
        FUNC_BGE  = 3'h5,
        FUNC_BLTU = 3'h6,
        FUNC_BGEU = 3'h7
    } branch_func3_t;
    typedef enum logic [2:0] {
        FUNC_LB  = 3'h0,
        FUNC_LH  = 3'h1,
        FUNC_LW  = 3'h2,
        FUNC_LBU = 3'h4,
        FUNC_LHU = 3'h5
    } load_func3_t;
    typedef enum logic [2:0] {
        FUNC_SB = 3'h0,
        FUNC_SH = 3'h1,
        FUNC_SW = 3'h2
    } store_func3_t;
    typedef enum logic [2:0] {
        FUNC_ADD  = 3'h0,    // ADD or SUB is determined by FUNC7
        FUNC_SLL  = 3'h1,
        FUNC_SLT  = 3'h2,
        FUNC_SLTU = 3'h3,
        FUNC_XOR  = 3'h4,
        FUNC_SRLA = 3'h5,    // Arithmetic (SRA) or logical (SRL) determined by FUNC7
        FUNC_OR   = 3'h6,
        FUNC_AND  = 3'h7
    } op_func3_t;
    typedef enum logic [2:0] {
        FUNC_MUL    = 3'h0,
        FUNC_MULH   = 3'h1,
        FUNC_MULHSU = 3'h2,
        FUNC_MULHU  = 3'h3,
        FUNC_DIV    = 3'h4,
        FUNC_DIVU   = 3'h5,
        FUNC_REM    = 3'h6,
        FUNC_REMU   = 3'h7
    } mul_func3_t;
    typedef enum logic [2:0] {
        FUNC_FENCE   = 3'h0,
        FUNC_FENCE_I = 3'h1
    } misc_mem_func3_t;
    typedef enum logic [2:0] {
        FUNC_ECALL_EBREAK = 3'h0,    // ECALL and EBREAK instructions are determined by imm[11:0]
        FUNC_CSRRW        = 3'h1,
        FUNC_CSRRS        = 3'h2,
        FUNC_CSRRC        = 3'h3,
        FUNC_CSRRWI       = 3'h5,
        FUNC_CSRRSI       = 3'h6,
        FUNC_CSRRCI       = 3'h7
    } system_func3_t;
    typedef enum logic [2:0] {
        FUNC_MIN    = 3'h4,
        FUNC_MINU   = 3'h5,
        FUNC_MAX    = 3'h6,
        FUNC_MAXU   = 3'h7
    } minmax_clmul_func3_t;

    // Overall instruction format
    typedef struct packed {
        logic [24:0] tail;
        logic [6:0] opcode;
    } instruction_t;

    typedef enum logic [2:0] {
        R_TYPE, I_TYPE, S_TYPE, B_TYPE, U_TYPE, J_TYPE
    } decode_type_t;

    // Utility functions on instructions
    function automatic decode_type_t decode_type (instruction_t instruction);
        case (instruction.opcode)
            OPCODE_OP                                                               : return R_TYPE;
            OPCODE_SYSTEM, OPCODE_MISC_MEM, OPCODE_LOAD, OPCODE_JALR, OPCODE_OP_IMM : return I_TYPE;
            OPCODE_AUIPC, OPCODE_LUI                                                : return U_TYPE;
            OPCODE_JAL                                                              : return J_TYPE;
            OPCODE_BRANCH                                                           : return B_TYPE;
            OPCODE_STORE                                                            : return S_TYPE;
            default                                                                 : return decode_type_t'('x);
        endcase
    endfunction

    // Utility functions on instructions
    function automatic logic [31:0] get_immediate (instruction_t instruction, decode_type_t inst_type);
        case (inst_type)
            I_TYPE  : return i_immediate(instruction_i_t'(instruction.tail));
            U_TYPE  : return u_immediate(instruction_u_t'(instruction.tail));
            J_TYPE  : return j_immediate(instruction_j_t'(instruction.tail));
            B_TYPE  : return b_immediate(instruction_b_t'(instruction.tail));
            S_TYPE  : return s_immediate(instruction_s_t'(instruction.tail));
            default : return 'x;
        endcase
    endfunction

    // Getters for instruction destination and source registers - rely on the fact that these are always
    // in the same place for every instruction
    function automatic logic [4:0] get_dest (instruction_t instruction);
        instruction_r_t r_type = instruction_r_t'(instruction.tail);
        return r_type.rd;
    endfunction
    function automatic logic [4:0] get_src_a (instruction_t instruction);
        instruction_r_t r_type = instruction_r_t'(instruction.tail);
        return r_type.rs1;
    endfunction
    function automatic logic [4:0] get_src_b (instruction_t instruction);
        instruction_r_t r_type = instruction_r_t'(instruction.tail);
        return r_type.rs2;
    endfunction

    // Get the func3 and func7 for an instruction
    function automatic branch_func3_t get_b_func3(instruction_t instruction);
        instruction_b_t b_type = instruction_b_t'(instruction.tail);
        return branch_func3_t'(b_type.func3);
    endfunction
    function automatic op_func3_t get_op_func3(instruction_t instruction);
        instruction_r_t r_type = instruction_r_t'(instruction.tail);
        return op_func3_t'(r_type.func3);
    endfunction
    function automatic minmax_clmul_func3_t get_minmax_clmul_func3(instruction_t instruction);
        instruction_r_t r_type = instruction_r_t'(instruction.tail);
        return minmax_clmul_func3_t'(r_type.func3);
    endfunction
    function automatic logic[6:0] get_func7(instruction_t instruction);
        instruction_r_t r_type = instruction_r_t'(instruction.tail);
        return r_type.func7;
    endfunction
    function automatic logic[2:0] get_i_func3_bits(instruction_t instruction);
        instruction_i_t i_type = instruction_i_t'(instruction.tail);
        return i_type.func3;
    endfunction
    function automatic logic[2:0] get_s_func3_bits(instruction_t instruction);
        instruction_s_t s_type = instruction_s_t'(instruction.tail);
        return s_type.func3;
    endfunction

endpackage